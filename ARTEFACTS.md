# ARTEFACTS

- [WIP] gallery: ability to show mixed content of photos + videos (idem stories)

- find another comment system ?

- might be able to get rid of animejs dependency (used in tabs/index.tsx) to reduce app size
  - see scrollBy usage in text-components/Summary.tsx

## ROADMAP

- seuls nos corps: open pictures in same page using `Gallery` component?

## INCREMENT

- ability to handle various routes
- ability to switch between nature and graffiti themes
- ability to display a list of videos from youtube and/or vimeo
- ability to switch between french and english languages
- ability to easily deploy the app
- responsive design
- handle https
- update `<head>` attributes depending on context (page, tab), for SEO friendliness
- ability to access to a catalog
- catalog tab presence depends on a feature flag fed by the backend
- catalog route renders to another tab (videos) when feature is disabled
- tech: TypeScript support
- ability to fetch and display texts, and navigate through their chapters
- ability to preview texts
- ability to translate texts
- ability to attach text to video
