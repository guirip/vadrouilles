const { override, addBabelPlugins } = require('customize-cra')

module.exports = override(
  addBabelPlugins(
    [
      'babel-plugin-styled-components',
      {
        displayName: true
      }
    ],
    [ 'module-resolver', {
      'root': ['./src'],
      'alias': {
        // NB: add any new alias to ./jsconfig.json too !
        'src': './src',
      }
    }]
  )
)
