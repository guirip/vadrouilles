

### Prevent apache from trying to handle paths managed react-router

e.g for url: https://vadrouilles.fr/graffiti/seuls-nos-corps
the default apache behaviour would be to look for an index file in 'seuls-nos-corps' folder, which does not exist.

I've been pulling my hair off for a (way too) long time.

**First important notice: The rewrite rule must be placed in the Virtual host for port 443**

**Second important notice: I've not been able to use the common apache configuration samples (HTTP 400), the only working configuration applies for all the files. The first RewriteRule is not very nice but prevents the RewriteRule from applying on existing files...**


    <VirtualHost *:443>
        ServerName vadrouilles.fr
        DocumentRoot /var/www/vadrouilles.fr/current

        <Directory /var/www/vadrouilles.fr/current>
                Require all granted
        </Directory>

        RewriteRule \. - [L]   # no rewrite rule applied for any path containing a dot
        RewriteEngine On
        RewriteRule ^index\.html$ - [L]
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteCond %{REQUEST_FILENAME} !-l
        RewriteRule . /index.html [L]

        ...
