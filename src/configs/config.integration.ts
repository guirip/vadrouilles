import { IConfig } from './config.default'

const integrationConf: Partial<IConfig> = {}

export default integrationConf
