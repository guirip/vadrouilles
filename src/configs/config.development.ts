import { IConfig } from './config.default'

const devConf: Partial<IConfig> = {
  CODIFS_REFRESH: 6000,
  BACKEND_URL: `http://${window.location.hostname}:3000`,
  // DISQUS_SHORT_NAME: 'vadrouilles-dev-fr',
}

export default devConf
