import { Category } from 'src/types/Category'

export interface ISocialConfig {
  name: string
  url: string
}

export interface IConfig {
  FETCH_TIMEOUT: number
  CODIFS_REFRESH: number
  GET_CATALOG_PATH: string
  GET_CODIF_PATH: string
  GET_VIDEO_PATH: string
  GET_TEXT_PATH: string
  GET_CHAPTER_PATH: string
  GET_LOCALIZED_TEXT_PATH: string
  BACKEND_URL: string
  MY_EMAIL: string
  MY_INSTAGRAM: {
    [Category.NATURE]: ISocialConfig
    [Category.GRAFFITI]: ISocialConfig
    [Category.ABOUT]: ISocialConfig
  }
  MY_YOUTUBE: {
    [Category.NATURE]: ISocialConfig
    [Category.GRAFFITI]: ISocialConfig
    [Category.ABOUT]: ISocialConfig
  }
  DISQUS_SHORT_NAME: string
}

const defaultConf: Partial<IConfig> = {
  FETCH_TIMEOUT: 5000,
  CODIFS_REFRESH: 30000,
  GET_CATALOG_PATH: '/api/catalog-item/visible',
  GET_CODIF_PATH: '/api/codif',
  GET_VIDEO_PATH: '/api/video',
  GET_TEXT_PATH: '/api/text/page',
  GET_CHAPTER_PATH: '/api/text/chapter',
  GET_LOCALIZED_TEXT_PATH: '/api/text/localized-text',
  BACKEND_URL: '/backend',
  MY_EMAIL: 'grigri@hacari.net',
  MY_INSTAGRAM: {
    [Category.NATURE]: {
      name: 'Vadrouilles.fr',
      url: 'https://www.instagram.com/vadrouilles.fr/',
    },
    [Category.GRAFFITI]: {
      name: 'Sulpeb',
      url: 'https://www.instagram.com/sulpeb/',
    },
    [Category.ABOUT]: {
      name: '',
      url: '',
    },
  },
  MY_YOUTUBE: {
    [Category.NATURE]: {
      name: 'Vadrouilles attentives',
      url: 'https://www.youtube.com/@vadrouillesattentives',
    },
    [Category.GRAFFITI]: {
      name: 'Beplus graffiti',
      url: 'https://www.youtube.com/@beplus.graffiti',
    },
    [Category.ABOUT]: {
      name: '',
      url: '',
    },
  },
}

export default defaultConf
