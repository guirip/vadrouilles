import { IFetchResult } from './Utils'

export default async function fetchWrapper<T>(
  promise: Promise<IFetchResult>,
  errorCb: (error: unknown) => void,
  cb: (data: T | null) => void
) {
  let response
  let error: unknown = null
  try {
    const result = await promise
    response = result.response
    error = result.error
  } catch (e) {
    error = e
  }

  if (response && response.ok) {
    try {
      const textResponse = await response.text()
      if (textResponse.trim().length === 0) {
        cb(null)
      } else {
        cb(JSON.parse(textResponse))
      }
      return
    } catch (e) {
      error = e
    }
  }
  errorCb(error)
}
