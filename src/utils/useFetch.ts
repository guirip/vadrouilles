import { useCallback, useState } from 'react'
import fetchWrapper from './fetchWrapper'
import { executeRequest, getBackendUrl } from './Utils'

export function useFetch<T>(
  url: string,
  opts?: RequestInit | undefined,
  queryParams?: Record<string, unknown>
): [boolean, () => void, unknown, T | null] {
  const [loading, setLoading] = useState<boolean>(false)
  const [error, setError] = useState<unknown>(null)
  const [data, setData] = useState<T | null>(null)

  const fetchFn = useCallback(() => {
    setLoading(true)
    setError(null)

    fetchWrapper<T>(
      executeRequest(getBackendUrl(url, queryParams), opts),
      (error) => {
        setLoading(false)
        setData(null)
        setError(error)
      },
      (receivedData) => {
        setLoading(false)
        setData(receivedData as unknown as T)
        setError(null)
      }
    )
  }, [url, opts, queryParams])

  return [loading, fetchFn, error, data]
}
