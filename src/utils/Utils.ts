import config from 'src/config'

export const isMobileDevice = () => 'ontouchstart' in document.documentElement

export const MOBILE_CSS_CLASS = 'mobile'
export function addCssClassIfMobile() {
  if (
    isMobileDevice() &&
    document.body.classList.contains(MOBILE_CSS_CLASS) !== true
  ) {
    document.body.classList.add(MOBILE_CSS_CLASS)
  }
}

const userAgent = navigator.userAgent
const isSafariBrowser =
  userAgent.indexOf('Chrome') === -1 && userAgent.indexOf('Safari') > -1

export const isSafari = () => isSafariBrowser

export const isDevEnv = () =>
  window.location.hostname === 'localhost' ||
  window.location.hostname.startsWith('192.168')

const getQueryString = (queryParams: Record<string, unknown>) =>
  '?' +
  Object.keys(queryParams)
    .map((key) => {
      const value = queryParams[key]
      return value !== undefined && value !== null ? `${key}=${value}` : null
    })
    .filter((i) => i)
    .join('&')

export const getBackendUrl = (
  path: string,
  queryParams?: Record<string, unknown>
) =>
  `${config.BACKEND_URL}${path}${
    queryParams ? getQueryString(queryParams) : ''
  }`

export interface IFetchResult {
  error?: string
  response?: Response
}

/**
 * Simple wrapper to handle errors in a generic way
 * @param  {string} url
 * @param  {object} opts
 * @return {object} response
 */
export const executeRequest = (
  url: string,
  opts?: RequestInit
): Promise<IFetchResult> =>
  new Promise(function (resolve, reject) {
    let didTimeout = false
    function handleTimeout() {
      didTimeout = true
      reject(new Error('Délai dépassé'))
    }
    const fetchTimeoutId = window.setTimeout(
      handleTimeout,
      config.FETCH_TIMEOUT || 10000
    )

    function clearFetchTimeout() {
      window.clearTimeout(fetchTimeoutId)
    }

    const options = Object.assign(
      {
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        // credentials: 'include',
      },
      opts
    )

    if (!options.method) {
      options.method = 'GET'
    }

    // console.log(`Calling ${url} with options `, options)

    fetch(url, options)
      .then(function (response) {
        clearFetchTimeout()

        if (response.ok !== true) {
          resolve({ error: `${response.status} ${response.statusText}` })
        } else {
          resolve({ response })
        }
      })
      .catch(function (error) {
        if (didTimeout) return
        clearFetchTimeout()
        resolve({ error })
      })
  })

const PX_REGEXP = /^(\d*)px$/

export function getStyleAsInteger(el: HTMLElement, props: string[]) {
  const values: Record<string, number> = {}
  const computed = window.getComputedStyle(el)
  for (const prop of props) {
    const result = PX_REGEXP.exec(computed.getPropertyValue(prop))
    values[prop] = !result || !result[1] ? 0 : parseInt(result[1], 10)
  }
  return values
}

export function stopEventPropagation(e: React.UIEvent) {
  e.stopPropagation()
}

export function getErrorMessage(error: unknown) {
  const errorMessage =
    error && typeof error === 'object' && 'message' in error
      ? error.message
      : null
  return errorMessage || error
}
