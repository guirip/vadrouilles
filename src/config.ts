import deepmerge from 'deepmerge'

import defaultConfig, { IConfig } from './configs/config.default'
import devConfig from './configs/config.development'
import integrationConfig from './configs/config.integration'
import prodConfig from './configs/config.production'

let mergedConfig: IConfig

enum Env {
  default = 'default',
  development = 'development',
  integration = 'integration',
  production = 'production',
}
const envs = Object.values(Env).map((e) => Env[e])

const configs = {
  [Env.default]: defaultConfig,
  [Env.development]: devConfig,
  [Env.integration]: integrationConfig,
  [Env.production]: prodConfig,
}

const paramEnv = process.env.CONFIG || process.env.NODE_ENV
const env = envs.find((e) => e === paramEnv)
if (!env) {
  console.error(`Invalid env: ${paramEnv}`)
  process.exit(1)
}

const envConfig = configs[env]
if (!envConfig) {
  console.error(`Missing configuration file for environment ${paramEnv}`)
  process.exit(1)
} else {
  mergedConfig = deepmerge(configs.default, envConfig)
}

export default mergedConfig
