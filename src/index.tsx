import React, { StrictMode } from 'react'
import { createRoot } from 'react-dom/client'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { addCssClassIfMobile } from './utils/Utils'

import 'primereact/resources/primereact.min.css'
import 'primereact/resources/themes/mdc-dark-indigo/theme.css'
import 'primeicons/primeicons.css'
import 'primeflex/primeflex.css'

import './fonts'
import './index.css'

addCssClassIfMobile()

const ROOT_EL_ID = 'root'

const rootEl = document.getElementById(ROOT_EL_ID)
if (!rootEl) {
  console.error(`Failed to find element #${ROOT_EL_ID}`)
} else {
  const reactRoot = createRoot(rootEl)
  reactRoot.render(
    <StrictMode>
      <App />
    </StrictMode>
  )

  // If you want your app to work offline and load faster, you can change
  // unregister() to register() below. Note this comes with some pitfalls.
  // Learn more about service workers: https://bit.ly/CRA-PWA
  serviceWorker.unregister()
}
