import './luckiestguy-font.css'
import './merriweather-font.css'
import './montserrat-font.css'
import './salsa-font.css'

export enum FontFamily {
  LUCKIESTGUY = 'LuckiestGuy',
  MONTSERRAT = 'Montserrat',
  MERRIWEATHER = 'Merriweather',
  SALSA = 'Salsa',
}
