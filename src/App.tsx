import React, { lazy, Suspense } from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'

import AppRoute from './routes/Route'
import Loader from './components/loader'
import ErrorBoundary from './components/ErrorBoundary'

import { LangContextProvider } from './context/LangContext'
import { CodifContextProvider } from './context/CodifContext'

import { DeviceContextProvider } from './context/DeviceContextProvider'

import './App.css'

const GenericPage = lazy(() => import('./routes/generic'))
const GraffitiPage = lazy(() => import('./routes/graffiti'))
const TextPreviewPage = lazy(() => import('./routes/text-preview'))

function App() {
  return (
    <DeviceContextProvider>
      <LangContextProvider>
        <CodifContextProvider>
          <ErrorBoundary>
            <Suspense fallback={<Loader />}>
              <BrowserRouter>
                <Routes>
                  {/* TEXT PREVIEW ROUTES */}
                  <Route
                    path={AppRoute.TEXT_PREVIEW + '/:textSlug/:chapterSlug'}
                    element={<TextPreviewPage />}
                  />
                  <Route
                    path={AppRoute.TEXT_PREVIEW + '/:textSlug'}
                    element={<TextPreviewPage />}
                  />

                  {/* GRAFFITI ROUTES */}
                  <Route
                    path={AppRoute.GRAFFITI + '/:currentTab/:subPath'}
                    element={<GraffitiPage />}
                  />
                  <Route
                    path={AppRoute.GRAFFITI + '/:currentTab'}
                    element={<GraffitiPage />}
                  />
                  <Route path={AppRoute.GRAFFITI} element={<GraffitiPage />} />

                  {/* GENERIC ROUTES */}
                  <Route
                    path={'/:route/:currentTab/:subPath'}
                    element={<GenericPage />}
                  />
                  <Route
                    path={'/:route/:currentTab'}
                    element={<GenericPage />}
                  />
                  <Route path={'/:route'} element={<GenericPage />} />
                  <Route path={'/'} element={<GenericPage route="nature" />} />
                </Routes>
              </BrowserRouter>
            </Suspense>
          </ErrorBoundary>
        </CodifContextProvider>
      </LangContextProvider>
    </DeviceContextProvider>
  )
}

export default App
