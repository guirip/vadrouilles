import { Lang } from 'src/lang/Lang'

export interface IAppLabels {
  lang: Lang
  isoCode: string
  [key: string]: any
}
