export enum CodifType {
  String = 'string',
  Boolean = 'boolean',
  Number = 'number',
}

interface IBaseCodif {
  _id: string
  name: string
  type: CodifType
}

export interface IStringCodif extends IBaseCodif {
  value: string
}

export interface INumberCodif extends IBaseCodif {
  value: number
}

export interface IBooleanCodif extends IBaseCodif {
  value: boolean
}

export type ICodif = IStringCodif | INumberCodif | IBooleanCodif
