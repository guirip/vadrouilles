export enum Category {
  NATURE = 'nature',
  GRAFFITI = 'graffiti',
  ABOUT = 'about',
}
