export enum VideoCategory {
  Graffiti = 'graffiti',
  Nature = 'nature',
}

export enum VideoSource {
  Youtube = 'youtube',
  Vimeo = 'vimeo',
  Dailymotion = 'dailymotion',
}

export interface IVideo {
  _id: string
  label: string
  value: string
  category: VideoCategory
  source: VideoSource
  visible: boolean
  order: number
  width: number | null
  height: number | null
  description: string
}

export const isVideo = (value: unknown): value is IVideo =>
  !!(
    value &&
    typeof value === 'object' &&
    'value' in value &&
    'source' in value &&
    typeof value.value === 'string' &&
    typeof value.source === 'string' &&
    Object.values(VideoSource).includes(value.source as VideoSource)
  )
