import { Lang } from 'src/lang/Lang'

export interface Error {
  message: string
}

export type ILocalization = Record<Lang, string>
