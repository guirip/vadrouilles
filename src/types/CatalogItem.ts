import { ILocalization } from './CommonTypes'
import { IFile } from './File'

export enum ItemStatus {
  Available = 'available',
  Booked = 'booked',
  Sold = 'sold',
}

export interface ICatalogItem {
  _id: string
  status: ItemStatus
  visibility: boolean
  price: number
  order: number
  title: string
  description?: string
  technique?: string
  dimensions?: string
  weight?: string
  createDate?: Date | null
  updateDate?: Date | null
  files?: IFile[]
  category: string
}

export interface ICatalogItemCategory {
  _id: string
  name: ILocalization
  slug: string
  order: number
  visibility: boolean
}

export interface IItemsPerCategory {
  [key: ICatalogItemCategory['_id']]: ICatalogItem[]
}
