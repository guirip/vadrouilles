import { Lang } from 'src/lang/Lang'
import { ILocalization } from './CommonTypes'
import { IFile } from './File'
import { IVideo } from './Video'

export enum TextCategory {
  NATURE = 'nature',
  GRAFFITI = 'graffiti',
}

// Content

export enum TextVariant {
  CALL_OUT = 'call-out',
  CITATION = 'citation',
  DETAILS = 'details',
}

export enum BlockType {
  Delimiter = 'delimiter',
  Gallery = 'gallery',
  Header = 'header',
  Image = 'image',
  List = 'list',
  Paragraph = 'paragraph',
  Spacer = 'spacer',
  Summary = 'summary',
  Table = 'table',
  Video = 'video',
}

export interface ITextContent {
  time: number
  blocks: IBlock[]
  version: string
}

export interface IBlock {
  id: string
  type: BlockType
  data?: object
}

export interface IDelimiterBlock extends IBlock {
  type: BlockType.Delimiter
}

export interface IHeaderBlock extends IBlock {
  type: BlockType.Header
  data: {
    text: string
    level: number
  }
}

export interface ITableBlock extends IBlock {
  type: BlockType.Table
  data: {
    withHeadings: boolean
    content: string[][]
  }
}

export interface ISummaryBlock extends IBlock {
  type: BlockType.Paragraph
  data: {
    headers: IHeaderBlock[]
  }
}

export interface INestedListItem {
  content: string
  items: INestedListItem[]
}

export interface IListBlock extends IBlock {
  type: BlockType.List
  data: {
    items: INestedListItem[]
    style: 'ordered' | 'unordered'
  }
}

export interface IParagraphBlock extends IBlock {
  type: BlockType.Paragraph
  data: {
    text: string
  }
  tunes: {
    textVariant: TextVariant
  }
}

export interface ISpacerBlock extends IBlock {
  type: BlockType.Spacer
  data: {
    value: number
  }
}

export interface IImageBlock extends IBlock {
  type: BlockType.Image
  data: {
    caption: string
    file: IFile
    stretched: boolean
    withBackground: boolean
    withBorder: boolean
  }
}

export interface IVideoBlock extends IBlock {
  type: BlockType.Video
  data: IVideo
}

export interface IGalleryBlock extends IBlock {
  type: BlockType.Gallery
  data: {
    showThumbnails?: boolean
    visuals: (IImageBlock | IVideoBlock)[]
  }
}

export const isDelimiterBlock = (block: IBlock): block is IDelimiterBlock =>
  block.type === BlockType.Delimiter

export const isGalleryBlock = (block: IBlock): block is IGalleryBlock =>
  block.type === BlockType.Gallery

export const isHeaderBlock = (block: IBlock): block is IHeaderBlock =>
  block.type === BlockType.Header

export const isImageBlock = (block: IBlock): block is IImageBlock =>
  block.type === BlockType.Image

export const isListBlock = (block: IBlock): block is IListBlock =>
  block.type === BlockType.List

export const isParagraphBlock = (block: IBlock): block is IParagraphBlock =>
  block.type === BlockType.Paragraph

export const isSpacerBlock = (block: IBlock): block is ISpacerBlock =>
  block.type === BlockType.Spacer

export const isSummaryBlock = (block: IBlock): block is ISummaryBlock =>
  block.type === BlockType.Summary

export const isVideoBlock = (block: IBlock): block is IVideoBlock =>
  block.type === BlockType.Video

export const isTableBlock = (block: IBlock): block is ITableBlock =>
  block.type === BlockType.Table

export interface ILocalizedText {
  _id: string
  title: string
  lang: Lang
  description: string
  content?: ITextContent
  blocksCount?: number
}

// Chapter

interface IBaseTextChapter {
  _id: string
  slug: string
  visible: boolean
  order: number
}
export interface ITextChapter extends IBaseTextChapter {
  localizedTexts: ILocalizedText['_id']
}
export interface ITextChapterPopulated extends IBaseTextChapter {
  localizedTexts: ILocalizedText[]
}

// Text

interface IBaseTextPage {
  _id: string
  slug: string
  title: ILocalization
  subtitle?: ILocalization
  visible: boolean
  description: string
}
export interface ITextPage extends IBaseTextPage {
  chapters: ITextChapter['_id']
}
export interface ITextPagePopulated extends IBaseTextPage {
  chapters: ITextChapterPopulated[]
}
