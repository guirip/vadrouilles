export interface IFile {
  orig: string
  mid: string
  low: string
  isMain?: boolean
}

export const isFile = (value: unknown): value is IFile =>
  typeof value === 'object' &&
  value !== null &&
  'mid' in value &&
  'low' in value
