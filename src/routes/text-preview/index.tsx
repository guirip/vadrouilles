import React, { createRef, useCallback, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import ErrorMessage from 'src/components/ErrorMessage'
import { Footer } from 'src/components/footer'
import Header from 'src/components/header'
import { TextContainer } from 'src/components/text/TextContainer'
import { createGlobalStyle } from 'styled-components'
import { Page, PageContent, ScrollableContent } from '../../style.main'
import Route from '../Route'
import { ScrollContext } from 'src/context/ScrollContext'
import { Category } from 'src/types/Category'

const GlobalStyle = createGlobalStyle`
  body {
  }
`

function TextPreviewPage() {
  const { textSlug, chapterSlug } = useParams()

  const contentRef = createRef<HTMLDivElement>()
  const scrollable = createRef<HTMLDivElement>()

  const resetScroll = useCallback(
    function () {
      if (scrollable.current) {
        // Reset scroll on tab change
        scrollable.current.scrollTop = 0
      }
    },
    [scrollable]
  )

  useEffect(() => {
    resetScroll()
  }, [scrollable, resetScroll])

  useEffect(() => {
    if (contentRef.current) {
      contentRef.current.scrollTop = 0
    }
  })

  return (
    <Page>
      <GlobalStyle />
      <Header currentRoute={Route.TEXT_PREVIEW} category={Category.NATURE} />
      <ScrollableContent ref={scrollable}>
        <ScrollContext.Provider value={{ ref: scrollable }}>
          <PageContent ref={contentRef}>
            {!textSlug ? (
              <ErrorMessage
                text={'Missing text slug'}
                errMessage={"Can't preview text"}
              />
            ) : (
              <TextContainer
                textSlug={textSlug}
                chapterSlug={chapterSlug}
                baseRoute={Route.TEXT_PREVIEW}
                resetScroll={resetScroll}
                visibility="all"
              />
            )}
          </PageContent>

          <Footer category={Category.NATURE} />
        </ScrollContext.Provider>
      </ScrollableContent>
    </Page>
  )
}

export default TextPreviewPage
