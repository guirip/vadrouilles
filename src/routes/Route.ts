enum Route {
  DEFAULT = '/',
  TEXT_PREVIEW = '/text-preview',

  NATURE = '/nature',

  GRAFFITI = '/graffiti',
  SEULS_NOS_CORPS = '/seuls-nos-corps',

  ABOUT = '/about',
}

const paths = Object.values(Route)

export const isRouteValid = (route: string): route is Route => {
  return paths.some((path: string) => path.includes(route))
}

export default Route
