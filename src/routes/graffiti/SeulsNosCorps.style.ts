import styled from 'styled-components'
import { TextBlock, Text, FONT_SIZE } from '../../style.main'

interface ISubContainerProps {
  isMobileDevice: boolean
}

export const SubContainer = styled.div<ISubContainerProps>`
  max-width: 1420px;
  flex-direction: column;
  margin: auto;

  & h2 {
    margin: 1.5em 0.5em 0.2em;
    font-size: 1.7em;
    text-align: center;
  }
  & h3 {
    margin: 2em 1em 0.5em;
    font-size: ${({ isMobileDevice }) => (isMobileDevice ? '1.3em' : '1.4em')};
    padding-left: 0.5em;
    text-align: center;
  }
`

export const IntroWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  line-height: 1.3rem;
`

export const Order = styled(TextBlock)`
  font-size: ${FONT_SIZE.smaller};
  line-height: 1.3rem;
`

export const Infos = styled(Text)`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 1em 0 0.5em;
`

export const TechnicalDetails = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  max-width: 700px;
  width: 100vw;

  & > div {
    margin-top: 1em;
  }

  & b {
    font-size: ${FONT_SIZE.smallest};
    font-weight: bold;
    text-decoration: underline;
  }

  & a {
    color: #56d5ff;
  }
  & .fa-instagram {
    font-size: ${FONT_SIZE.content};
  }

  & ul {
    margin: 0.2em;
    padding-left: 1em;
    list-style: none;
  }
  & li {
    margin: 0.4em 0;
  }
`
