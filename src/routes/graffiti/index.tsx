import React, { createRef, lazy, useCallback, useEffect, useMemo } from 'react'
import { Link, Navigate, useParams } from 'react-router-dom'
import styled from 'styled-components'

import { useCodifContext } from 'src/context/CodifContext'
import { useLangContext } from 'src/context/LangContext'

import Route from 'src/routes/Route'

import { Footer } from 'src/components/footer'
import Header from 'src/components/header'
import Tabs, { ITabData } from 'src/components/tabs'
import Tab from './GraffitiTab'

import ErrorMessage from 'src/components/ErrorMessage'
import { generateTabsForPages } from 'src/components/tabs/tab.util'
import { TextContainer } from 'src/components/text/TextContainer'
import { getDefaultPage, getTextPage } from 'src/components/text/text.util'
import config from 'src/config'
import { ScrollContext } from 'src/context/ScrollContext'
import { Category } from 'src/types/Category'
import { ITextPage, TextCategory } from 'src/types/Text'
import { useFetch } from 'src/utils/useFetch'
import { Page, PageContent, ScrollableContent } from '../../style.main'

const SeulsNosCorps = lazy(() => import('./SeulsNosCorps'))
const Catalog = lazy(() => import('../../components/catalog/index'))

const ContentWrapper = styled.div`
  width: 100vw;
`

const FETCH_PARAMS = {
  category: TextCategory.GRAFFITI,
}

function GraffitiPage() {
  const params = useParams()
  const currentTab = params.currentTab
  const { subPath } = params

  const scrollable = createRef<HTMLDivElement>()
  const { labels } = useLangContext()
  const { catalogStatus } = useCodifContext()
  const isCatalogStatusLoaded = catalogStatus !== null
  const isCatalogEnabled = catalogStatus === true

  const [, fetchTextPage, , textPages] = useFetch<ITextPage[]>(
    config.GET_TEXT_PATH,
    undefined,
    FETCH_PARAMS
  )

  useEffect(() => {
    fetchTextPage()
  }, [fetchTextPage])

  const resetScroll = useCallback(
    function () {
      if (scrollable.current) {
        // Reset scroll on tab change
        scrollable.current.scrollTop = 0
      }
    },
    [scrollable]
  )

  useEffect(() => {
    resetScroll()
  }, [currentTab, scrollable, resetScroll])

  const tabData = useMemo(() => {
    const tabs: ITabData[] = []

    if (isCatalogEnabled) {
      tabs.push({
        key: Tab.CATALOG,
        getTabContent: () => (
          <Link to={Route.GRAFFITI + '/' + Tab.CATALOG}>
            <div>{labels.catalog.tabTitle}</div>
          </Link>
        ),
        isCurrentTab: () => currentTab === Tab.CATALOG,
      })
    }

    return [
      ...tabs,
      ...generateTabsForPages(
        textPages ?? [],
        currentTab ||
          (Array.isArray(textPages) && textPages.length > 0
            ? getDefaultPage(textPages).slug
            : ''),
        Route.GRAFFITI,
        labels.lang
      ),
      {
        key: Tab.SEULS_NOS_CORPS,
        getTabContent: () => (
          <Link to={Route.GRAFFITI + '/' + Tab.SEULS_NOS_CORPS}>
            <div>{labels.seulsNosCorps.title}</div>
          </Link>
        ),
        isCurrentTab: () => currentTab === Tab.SEULS_NOS_CORPS,
      },
    ]
  }, [labels, isCatalogEnabled, currentTab, textPages])

  const pageContent = useMemo(() => {
    if (currentTab === Tab.CATALOG) {
      return <Catalog categorySlug={subPath} />
    }
    if (currentTab === Tab.SEULS_NOS_CORPS) {
      return <SeulsNosCorps />
    }
    const textPage = getTextPage(currentTab, textPages ?? [])
    if (!textPage) {
      return <ErrorMessage text={labels.text.fetchError} />
    }
    return (
      <TextContainer
        textSlug={textPage.slug}
        baseRoute={Route.GRAFFITI}
        chapterSlug={subPath}
        resetScroll={resetScroll}
        visibility={null}
      />
    )
  }, [currentTab, textPages, subPath, resetScroll, labels.text.fetchError])

  // Redirect to default tab when path points to catalog but feature is disabled
  if (
    currentTab === Tab.CATALOG &&
    isCatalogStatusLoaded &&
    !isCatalogEnabled
  ) {
    return <Navigate to={Route.GRAFFITI} replace />
  }

  return (
    <Page>
      <Header currentRoute={Route.GRAFFITI} category={Category.GRAFFITI} />
      <Tabs data={tabData} />

      <ScrollableContent ref={scrollable}>
        <ScrollContext.Provider value={{ ref: scrollable }}>
          <PageContent>
            <ContentWrapper>{pageContent}</ContentWrapper>
          </PageContent>

          <Footer category={Category.GRAFFITI} />
        </ScrollContext.Provider>
      </ScrollableContent>
    </Page>
  )
}

export default GraffitiPage
