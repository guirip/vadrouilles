import React from 'react'
import { Helmet } from 'react-helmet'

import { TextBlock } from '../../style.main'
import Gallery from 'src/components/gallery-simple'
import YouTubeVideo from 'src/components/video/YouTubeVideo'

import { useLangContext } from 'src/context/LangContext'
import {
  SubContainer,
  IntroWrap,
  Order,
  Infos,
  TechnicalDetails,
} from './SeulsNosCorps.style'
import { getData } from './SeulsNosCorps.data'
import { useDeviceContext } from 'src/context/DeviceContextProvider'
import { CoreContent } from 'src/components/core-content'

function SeulsNosCorps() {
  const { labels: allLabels } = useLangContext()
  const { isMobile } = useDeviceContext()
  const { seulsNosCorps: pageLabels } = allLabels

  return (
    <SubContainer isMobileDevice={isMobile}>
      <Helmet>
        <title>{`${pageLabels.title} - ${allLabels.appTitle}`}</title>
        <meta name="description" content={pageLabels.intro1} />
      </Helmet>

      <CoreContent>
        <IntroWrap>
          <TextBlock>
            <p dangerouslySetInnerHTML={{ __html: pageLabels.intro1 }} />
            <p>{pageLabels.intro2}</p>
            <p>{pageLabels.intro3}</p>
          </TextBlock>
        </IntroWrap>
        <Order dangerouslySetInnerHTML={{ __html: pageLabels.orderPossible }} />
        <h3>{pageLabels.galleryTitle}</h3>
        <Gallery data={getData(allLabels, pageLabels)} />
        <Infos>
          <TechnicalDetails>
            <div>
              <b>{pageLabels.supportLabel}</b> {pageLabels.support}
            </div>
            <div>
              <b>{pageLabels.sizeLabel}</b> {pageLabels.size}
            </div>
            <div>
              <b>{pageLabels.techniqueLabel}</b> {pageLabels.technique}
            </div>
            <div>
              <b>{pageLabels.saatoProject}</b>
              <ul>
                <li>
                  <i className="fab fa-instagram"></i>{' '}
                  <a href="https://www.instagram.com/notoriousbrandfr">
                    instagram.com/notoriousbrandfr
                  </a>
                </li>
                <li>
                  <i className="fas fa-external-link-alt"></i>{' '}
                  <a href="https://www.projetsaato.com/">www.projetsaato.com</a>
                </li>
              </ul>
            </div>
          </TechnicalDetails>
        </Infos>
        <h3>{pageLabels.processTitle}</h3>
        <TextBlock>{pageLabels.processText}</TextBlock>
        <YouTubeVideo id="-Pb8JjXK15Y" /> {/* oeuvre 3 */}
        <YouTubeVideo id="nqLDE6CHMy4" /> {/* oeuvre 9 */}
        <YouTubeVideo id="qOIYNd2BhHE" /> {/* oeuvre 10 */}
        <YouTubeVideo id="LnijsoKnfro" /> {/* oeuvre 12 */}
        <YouTubeVideo id="ZJ1RyvCxdgM" /> {/* oeuvre 13 */}
        <YouTubeVideo id="gK5frVZ1Xzw" /> {/* oeuvre 14 */}
        <YouTubeVideo id="ryXDhpMe9Kw" /> {/* oeuvre 15 */}
        <YouTubeVideo id="t9qhla2KOw0" /> {/* oeuvre 16 */}
      </CoreContent>
    </SubContainer>
  )
}

export default SeulsNosCorps
