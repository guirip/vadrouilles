import { IGalleryItem } from 'src/components/gallery-simple'
import { IAppLabels } from 'src/types/AppLabels'

/*
    description: pageLabels.status.available,
    descriptionClassName: 'color-light-green',
*/
export const getData = (allLabels: IAppLabels, pageLabels: any) => {
  const data: IGalleryItem[] = [
    {
      title: pageLabels.artwork + ' 1',
      description: pageLabels.status.unavailable,
      date: allLabels.month.apr + ' 2020',
      imageThumb: '/images/seuls-nos-corps-sont-confines/oeuvre-01.JPG',
      imageLarge: '/images/seuls-nos-corps-sont-confines/full/oeuvre-01.JPG',
    },
    {
      title: pageLabels.artwork + ' 2',
      description: `${pageLabels.status.sold} ・ <b>Bryan</b> <span class="emoji">✌</span>`,
      date: allLabels.month.apr + ' 2020',
      imageThumb: '/images/seuls-nos-corps-sont-confines/oeuvre-02.JPG',
      imageLarge: '/images/seuls-nos-corps-sont-confines/full/oeuvre-02.JPG',
    },
    {
      title: pageLabels.artwork + ' 3',
      description: pageLabels.status.soldForAPHP,
      date: allLabels.month.apr + ' 2020',
      imageThumb: '/images/seuls-nos-corps-sont-confines/oeuvre-03.JPG',
      imageLarge: '/images/seuls-nos-corps-sont-confines/full/oeuvre-03.JPG',
    },
    {
      title: pageLabels.artwork + ' 4',
      description: pageLabels.status.soldForAPHP,
      date: allLabels.month.apr + ' 2020',
      imageThumb: '/images/seuls-nos-corps-sont-confines/oeuvre-04.JPG',
      imageLarge: '/images/seuls-nos-corps-sont-confines/full/oeuvre-04.JPG',
    },
    {
      title: pageLabels.artwork + ' 5',
      description: pageLabels.status.soldForAPHP,
      date: allLabels.month.apr + ' 2020',
      imageThumb: '/images/seuls-nos-corps-sont-confines/oeuvre-05.JPG',
      imageLarge: '/images/seuls-nos-corps-sont-confines/full/oeuvre-05.JPG',
    },
    {
      title: pageLabels.artwork + ' 6',
      description: allLabels.for + ' <b>Etienne</b>',
      date: allLabels.month.apr + ' 2020',
      imageThumb: '/images/seuls-nos-corps-sont-confines/oeuvre-06.JPG',
      imageLarge: '/images/seuls-nos-corps-sont-confines/full/oeuvre-06.JPG',
    },
    {
      title: pageLabels.artwork + ' 7',
      description: pageLabels.status.soldForAPHP,
      date: allLabels.month.apr + ' 2020',
      imageThumb: '/images/seuls-nos-corps-sont-confines/oeuvre-07.JPG',
      imageLarge: '/images/seuls-nos-corps-sont-confines/full/oeuvre-07.JPG',
    },
    {
      title: pageLabels.artwork + ' 8',
      description: pageLabels.status.soldForAPHP,
      date: allLabels.month.may + ' 2020',
      imageThumb: '/images/seuls-nos-corps-sont-confines/oeuvre-08.JPG',
      imageLarge: '/images/seuls-nos-corps-sont-confines/full/oeuvre-08.JPG',
    },
    {
      title: pageLabels.artwork + ' 9',
      description:
        pageLabels.status.sold +
        ' ・ <b>Merci Céline</b> <span class="emoji">😉</span>',
      date: allLabels.month.may + ' 2020',
      imageThumb: '/images/seuls-nos-corps-sont-confines/oeuvre-09.JPG',
      imageLarge: '/images/seuls-nos-corps-sont-confines/full/oeuvre-09.JPG',
    },
    {
      title: pageLabels.artwork + ' 10',
      description:
        pageLabels.status.command +
        ' ・ <b>Merci Aurélie</b> <span class="emoji">🙏</span>',
      date: allLabels.month.may + ' 2020',
      imageThumb: '/images/seuls-nos-corps-sont-confines/oeuvre-10.JPG',
      imageLarge: '/images/seuls-nos-corps-sont-confines/full/oeuvre-10.JPG',
    },
    {
      title: pageLabels.artwork + ' 11',
      description: pageLabels.status.seeCatalog,
      descriptionClassName: 'color-light-green',
      date: allLabels.month.may + ' 2020',
      imageThumb: '/images/seuls-nos-corps-sont-confines/oeuvre-11.JPG',
      imageLarge: '/images/seuls-nos-corps-sont-confines/full/oeuvre-11.JPG',
    },
    {
      title: pageLabels.artwork + ' 12',
      description:
        pageLabels.status.command +
        ' ・ <b>Merci Laurent</b> <span class="emoji">🙏</span> ' +
        pageLabels.oldParisMapA4,
      date: allLabels.month.may + ' 2020',
      imageThumb: '/images/seuls-nos-corps-sont-confines/oeuvre-12.JPG',
      imageLarge: '/images/seuls-nos-corps-sont-confines/full/oeuvre-12.JPG',
    },
    {
      title: pageLabels.artwork + ' 13',
      description:
        pageLabels.status.sold +
        ' ・ <b>Merci Florence</b> <span class="emoji">😊</span>',
      date: allLabels.month.may + ' 2020',
      imageThumb: '/images/seuls-nos-corps-sont-confines/oeuvre-13.JPG',
      imageLarge: '/images/seuls-nos-corps-sont-confines/full/oeuvre-13.JPG',
    },
    {
      title: pageLabels.artwork + ' 14',
      description:
        pageLabels.status.command +
        ' ・ <b>Merci Florence</b> <span class="emoji">🙏</span>' +
        pageLabels.oldParisTarideMap1973,
      date: `${allLabels.month.may}/${allLabels.month.jun} 2020`,
      imageThumb: '/images/seuls-nos-corps-sont-confines/oeuvre-14.JPG',
      imageLarge: '/images/seuls-nos-corps-sont-confines/full/oeuvre-14.JPG',
    },
    {
      title: pageLabels.artwork + ' 15',
      description:
        pageLabels.status.command +
        ' ・ <b>Merci Céline</b> <span class="emoji">😉</span>' +
        pageLabels.oldMichelinMap1958,
      date: `${allLabels.month.may}/${allLabels.month.jun} 2020`,
      imageThumb: '/images/seuls-nos-corps-sont-confines/oeuvre-15.JPG',
      imageLarge: '/images/seuls-nos-corps-sont-confines/full/oeuvre-15.JPG',
    },
    {
      title: pageLabels.artwork + ' 16',
      description:
        pageLabels.status.command +
        ' ・ <b>Merci Carine</b> <span class="emoji">✌</span>',
      date: `${allLabels.month.aug} 2020`,
      imageThumb: '/images/seuls-nos-corps-sont-confines/oeuvre-16.JPG',
      imageLarge: '/images/seuls-nos-corps-sont-confines/full/oeuvre-16.JPG',
    },
  ]
  return data
}
