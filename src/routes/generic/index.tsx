import React, { createRef, useCallback, useEffect, useMemo } from 'react'
import { createGlobalStyle } from 'styled-components'

import { Page, PageContent, ScrollableContent } from '../../style.main'

import config from 'src/config'
import { useLangContext } from 'src/context/LangContext'
import { useFetch } from 'src/utils/useFetch'

import { Footer } from 'src/components/footer'
import Header from 'src/components/header'
import Tabs from 'src/components/tabs'

import ErrorMessage from 'src/components/ErrorMessage'
import Loader, { LoaderSize } from 'src/components/loader'
import { generateTabsForPages } from 'src/components/tabs/tab.util'
import { TextContainer } from 'src/components/text/TextContainer'
import { getDefaultPage, getTextPage } from 'src/components/text/text.util'
import { ScrollContext } from 'src/context/ScrollContext'
import { Category } from 'src/types/Category'
import { ITextPage } from 'src/types/Text'
import { useGenericRoute } from './generic-route.hook'

const GlobalStyle = createGlobalStyle`
  body {
    background-image: url(/images/background-nature.jpg);
  }
`

interface IProps {
  route?: string // used to handle default / path
}

function GenericPage(props: IProps) {
  const { route, routeWithSlash, isRouteOk, currentTab, subPath } =
    useGenericRoute(props.route)

  const fetchParams = useMemo(() => ({ category: route }), [route])

  const contentRef = createRef<HTMLDivElement>()
  const scrollable = createRef<HTMLDivElement>()
  const { labels } = useLangContext()

  const [loading, fetchTextPage, , textPages] = useFetch<ITextPage[]>(
    config.GET_TEXT_PATH,
    undefined,
    fetchParams
  )

  useEffect(() => {
    // Skip fetch if category is not set
    if (fetchParams.category) {
      fetchTextPage()
    }
  }, [fetchParams.category, fetchTextPage])

  const resetScroll = useCallback(
    function () {
      if (scrollable.current) {
        // Reset scroll on tab change
        scrollable.current.scrollTop = 0
      }
    },
    [scrollable]
  )

  useEffect(() => {
    resetScroll()
  }, [currentTab, scrollable, resetScroll])

  useEffect(() => {
    if (contentRef.current) {
      contentRef.current.scrollTop = 0
    }
  })

  const tabData = useMemo(() => {
    return [
      ...generateTabsForPages(
        textPages ?? [],
        currentTab ||
          (Array.isArray(textPages) && textPages.length > 0
            ? getDefaultPage(textPages).slug
            : ''),
        routeWithSlash ?? '',
        labels.lang
      ),
    ]
  }, [labels, currentTab, textPages, routeWithSlash])

  const pageContent = useMemo(() => {
    if (!routeWithSlash || !isRouteOk) {
      return null
    }
    if (loading) {
      return <Loader size={LoaderSize.MEDIUM} fadeDuration={500} />
    }
    const textPage = getTextPage(currentTab, textPages ?? [])
    if (!textPage) {
      return <ErrorMessage text={labels.text.fetchError} />
    }
    return (
      <TextContainer
        textSlug={textPage.slug}
        baseRoute={routeWithSlash}
        chapterSlug={subPath}
        resetScroll={resetScroll}
        visibility={null}
      />
    )
  }, [
    routeWithSlash,
    isRouteOk,
    loading,
    currentTab,
    textPages,
    subPath,
    resetScroll,
    labels.text.fetchError,
  ])

  if (!routeWithSlash || !isRouteOk) {
    return <ErrorMessage text={labels.pageNotFound} />
  }

  return (
    <Page>
      <GlobalStyle />
      <Header currentRoute={routeWithSlash} category={Category.NATURE} />
      {tabData.length > 1 && <Tabs data={tabData} />}

      <ScrollableContent ref={scrollable}>
        <ScrollContext.Provider value={{ ref: scrollable }}>
          <PageContent ref={contentRef}>{pageContent}</PageContent>

          <Footer category={Category.NATURE} />
        </ScrollContext.Provider>
      </ScrollableContent>
    </Page>
  )
}

export default GenericPage
