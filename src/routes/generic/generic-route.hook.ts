import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { isRouteValid } from '../Route'

export function useGenericRoute(fallBackRoute: string | undefined) {
  const params = useParams()
  const currentTab = params.currentTab ?? ''
  const { subPath } = params

  const [route, setRoute] = useState<string | undefined>(undefined)
  const [routeWithSlash, setRouteWithSlash] = useState<string | undefined>(
    undefined
  )
  const [isRouteOk, setRouteOk] = useState<boolean>(true)

  useEffect(() => {
    const route = params.route ?? fallBackRoute
    setRoute(route)
    setRouteWithSlash(route ? `/${route}` : undefined)
    setRouteOk(typeof route === 'string' && isRouteValid(route))
  }, [params.route, fallBackRoute])

  return {
    currentTab,
    subPath,
    route,
    routeWithSlash,
    isRouteOk,
  }
}
