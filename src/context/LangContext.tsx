import React, {
  createContext,
  PropsWithChildren,
  useContext,
  useState,
} from 'react'
import { IAppLabels } from 'src/types/AppLabels'
import { getLabels, setCurrentLang } from '../lang'
import { Lang } from 'src/lang/Lang'

export interface ILangContext {
  labels: IAppLabels
  updateCurrentLang: (lang: Lang) => void
}

const LangContext = createContext<ILangContext>({
  labels: getLabels(),
  updateCurrentLang: function (lang: Lang): void {
    console.log(`Not implemented yet. Got lang ${lang}`)
  },
})

export const useLangContext = () => useContext(LangContext)

export const LangContextProvider = ({ children }: PropsWithChildren) => {
  const [labels, setLabels] = useState(getLabels())

  function updateCurrentLang(lang: Lang) {
    setCurrentLang(lang)
    setLabels(getLabels())
  }

  return (
    <LangContext.Provider value={{ labels, updateCurrentLang }}>
      {children}
    </LangContext.Provider>
  )
}

export default LangContext
