import React, {
  createContext,
  PropsWithChildren,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react'
import { useInterval } from 'usehooks-ts'

import config from 'src/config'
import {
  IBooleanCodif,
  ICodif,
  INumberCodif,
  IStringCodif,
} from 'src/types/Codif'

enum CodifName {
  STATIC_URL = 'static-url',
  CATALOG_STATUS = 'catalog-status',
  CATALOG_NOTICE = 'catalog-notice',
  CATALOG_DISCOUNT = 'catalog-discount',
}

const codifsToRetrieve = [
  CodifName.STATIC_URL,
  CodifName.CATALOG_STATUS,
  CodifName.CATALOG_NOTICE,
  CodifName.CATALOG_DISCOUNT,
]
const PATH = `${config.BACKEND_URL}${
  config.GET_CODIF_PATH
}?name=${codifsToRetrieve.join(',')}&lean=true`

interface ICodifContext {
  staticUrl: string | null
  catalogStatus: boolean | null
  catalogNotice: string | null
  catalogDiscount: number | null
}

const CodifContext = createContext<ICodifContext>({
  staticUrl: null,
  catalogStatus: null,
  catalogNotice: null,
  catalogDiscount: null,
})

export const useCodifContext = () => useContext(CodifContext)

export async function fetchCodifs() {
  const response = await fetch(PATH)
  if (response) {
    const codifs = (await response.json()) as ICodif[]
    if (codifs) {
      return codifs
    }
  }
  return []
}

const getCodif = (codifs: ICodif[], name: CodifName) =>
  codifs.find((c) => c.name === name)

const MINIMUM_INTERVAL = 1000 * 60 // 60 sec

const canFetch = (d: Date) => d.getTime() + MINIMUM_INTERVAL < Date.now()

export const CodifContextProvider = ({ children }: PropsWithChildren) => {
  const [lastFetch, setLastFetch] = useState<Date | null>(null)

  // Codifs
  const [staticUrl, setStaticUrl] = useState<string | null>(null)
  const [catalogStatus, setCatalogStatus] = useState<boolean | null>(null)
  const [catalogNotice, setCatalogNotice] = useState<string | null>(null)
  const [catalogDiscount, setCatalogDiscount] = useState<number | null>(null)

  const refreshCodifs = useCallback(async () => {
    if (lastFetch && !canFetch(lastFetch)) {
      return
    }
    try {
      const codifs = await fetchCodifs()

      setStaticUrl(
        (getCodif(codifs, CodifName.STATIC_URL) as IStringCodif)?.value ?? null
      )
      setCatalogStatus(
        (getCodif(codifs, CodifName.CATALOG_STATUS) as IBooleanCodif)?.value ??
          null
      )
      setCatalogNotice(
        (getCodif(codifs, CodifName.CATALOG_NOTICE) as IStringCodif)?.value ??
          null
      )
      setCatalogDiscount(
        (getCodif(codifs, CodifName.CATALOG_DISCOUNT) as INumberCodif)?.value ??
          null
      )
    } catch (err) {
      console.error('Failed to fetch codifs', err)
    }
    setLastFetch(new Date())
  }, [
    lastFetch,
    setStaticUrl,
    setCatalogStatus,
    setCatalogNotice,
    setCatalogDiscount,
  ])

  useEffect(() => {
    refreshCodifs()
  }, [refreshCodifs])

  useInterval(refreshCodifs, config.CODIFS_REFRESH)

  const contextValue = useMemo(() => {
    return {
      staticUrl,
      catalogStatus,
      catalogNotice,
      catalogDiscount,
    }
  }, [staticUrl, catalogStatus, catalogNotice, catalogDiscount])

  return (
    <CodifContext.Provider value={contextValue}>
      {children}
    </CodifContext.Provider>
  )
}

export default CodifContext
