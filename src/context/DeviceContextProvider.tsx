import React, { createContext, PropsWithChildren, useContext } from 'react'
import { isMobileDevice } from 'src/utils/Utils'

interface IDeviceContext {
  isMobile: boolean
}

const defaultValue: IDeviceContext = {
  isMobile: isMobileDevice(),
}

const DeviceContext = createContext<IDeviceContext>(defaultValue)

export const useDeviceContext = () => useContext(DeviceContext)

export const DeviceContextProvider = ({ children }: PropsWithChildren) => (
  <DeviceContext.Provider value={defaultValue}>
    {children}
  </DeviceContext.Provider>
)
