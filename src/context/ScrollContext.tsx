import { createContext, useContext } from 'react'

interface IScrollContext {
  ref?: React.RefObject<HTMLDivElement>
}

export const ScrollContext = createContext<IScrollContext>({})

export const useScrollContext = () => useContext(ScrollContext)
