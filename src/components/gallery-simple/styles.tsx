import { FONT_SIZE } from 'src/style.main'
import styled from 'styled-components'

export const StyledGallery = styled.div`
  display: flex;
  justify-content: space-evenly;
  flex-wrap: wrap;
`

export const Item = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0.6em 0.4em;
  padding: 0 0.5em 0.5em;
  background-color: rgba(0, 0, 0, 0.35);

  & h4 {
    margin-bottom: 0.5em;
  }
`

export const ItemInfo = styled.div`
  margin: 0.2em 0;
  text-align: center;

  & div {
    padding: 0.2em;
    font-size: ${FONT_SIZE.smallest};
  }
`

export const Thumb = styled.div`
  flex-grow: 1;
  display: flex;
  align-items: center;
  margin: 0.5em;
`

export function determineThumbWidth() {
  const { clientWidth } = document.documentElement
  if (clientWidth < 500) {
    return clientWidth - 60
  }
  if (clientWidth < 1000) {
    return 420
  }
  if (clientWidth < 1400) {
    return 380
  }
  return 420
}
