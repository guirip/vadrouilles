import React, { useState } from 'react'
import {
  determineThumbWidth,
  StyledGallery,
  Item,
  ItemInfo,
  Thumb,
} from './styles'

export interface IGalleryItem {
  title: string
  description: string
  descriptionClassName?: string
  date: string
  imageThumb: string
  imageLarge: string
}

interface IProps {
  data: IGalleryItem[]
}

function Gallery({ data }: IProps) {
  const [thumbWidth, setThumWidth] = useState(determineThumbWidth())

  let canHandle = true
  function setCanHandle(value: boolean) {
    canHandle = value
  }
  function handleResize() {
    canHandle = false

    const newThumbWidth = determineThumbWidth()
    if (newThumbWidth !== thumbWidth) {
      setThumWidth(newThumbWidth)
    }
    window.setTimeout(setCanHandle, 240, true)
  }
  function onResize() {
    if (canHandle !== true) {
      return
    }
    window.setTimeout(handleResize, 50)
  }
  window.addEventListener('resize', onResize)
  window.addEventListener('orientationchange', onResize)

  return (
    <StyledGallery>
      {data.map((item) => (
        <Item key={item.title}>
          <h4>{item.title}</h4>
          <ItemInfo>
            <div>{item.date}</div>
            <div
              className={item.descriptionClassName || undefined}
              dangerouslySetInnerHTML={{ __html: item.description }}
            />
          </ItemInfo>
          <Thumb>
            <a href={item.imageLarge}>
              <img src={item.imageThumb} width={thumbWidth} alt={item.title} />
            </a>
          </Thumb>
        </Item>
      ))}
    </StyledGallery>
  )
}

export default Gallery
