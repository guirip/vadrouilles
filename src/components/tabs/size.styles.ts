export enum Size {
  NORMAL = 'normal',
  SMALLER = 'smaller',
}
const DEFAULT_SIZE = Size.NORMAL

interface ISizeStyle {
  h2: {
    height: string
  }
  a: {
    padding: string
    fontSize: string
    letterSpacing: string
  }
}

const SIZE_STYLE: Record<Size, ISizeStyle> = {
  [Size.NORMAL]: {
    h2: {
      height: '50px',
    },
    a: {
      padding: '.4rem 1.5rem',
      fontSize: '1rem',
      letterSpacing: '0',
    },
  },
  [Size.SMALLER]: {
    h2: {
      height: '36px',
    },
    a: {
      padding: '0 1.5rem',
      fontSize: '.85rem',
      letterSpacing: '1px',
    },
  },
}

export const getSizeStyle = (size?: Size) => SIZE_STYLE[size || DEFAULT_SIZE]
