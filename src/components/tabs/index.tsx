import React, { useEffect, useRef, useState } from 'react'
import anime from 'animejs'

import { getStyleAsInteger } from 'src/utils/Utils'
import { ScrollableTabsContainer, StyledTabs, Tab, TabContent } from './styles'
import { Mode } from './mode.style'
import { Size } from './size.styles'

declare global {
  function animeScroll(value: number): void
}

const MINIMUM_MOVE = 15 // pixels

const DEBUG = false

export interface ITabData {
  key: string
  label?: string
  getTabContent: () => JSX.Element
  isCurrentTab: () => boolean
  isClickable?: () => boolean
}

interface IProps {
  data: ITabData[]
  onTabIndexChange?: (index: number, key: string) => void
  size?: Size
  mode?: Mode
}

function Tabs({ data, onTabIndexChange, size, mode }: IProps) {
  const containerRef = useRef<HTMLDivElement>(null)
  const tabsRef = useRef<HTMLDivElement>(null)

  const [currentScrolledTabIndex, setCurrentScrolledTabIndex] =
    useState<number>(-1)
  const [scrollLeft, setScrollLeft] = useState(0)

  const scrollTimeout = useRef<number | null>()

  let xMaxThreshold: number
  const tabEls: (HTMLSpanElement | null)[] = []

  useEffect(() => {
    scrollTimeout.current = window.setTimeout(initScroll, 500)
    return () => {
      if (scrollTimeout.current) {
        window.clearTimeout(scrollTimeout.current)
        scrollTimeout.current = null
      }
    }
  })

  useEffect(() => {
    animeScroll(scrollLeft)
  }, [scrollLeft])

  if (!Array.isArray(data) || data.length === 0) {
    return null
  }

  function setTabEl(el: HTMLSpanElement | null, index: number) {
    tabEls[index] = el
  }

  function initScroll() {
    if (!Array.isArray(data) || data.length === 0 || !tabsRef.current) {
      return
    }

    const tabsWidth = tabsRef.current.offsetWidth
    const windowWidth = document.documentElement.clientWidth
    if (tabsWidth < windowWidth) {
      // No auto scroll when there is no x overflow
      xMaxThreshold = 0
    } else {
      xMaxThreshold = tabsWidth - windowWidth + 15 + windowWidth * 0.16 // page style margin: 8vw
    }

    if (DEBUG) {
      console.log(
        `[initScroll] tabsWidth:${tabsWidth} / windowWidth:${windowWidth} / xMaxThreshold:${xMaxThreshold}`
      )
    }

    // Autoscroll current tab
    const currentTabIndex = data.findIndex((tabData) => tabData.isCurrentTab())
    autoScroll(currentTabIndex)
  }

  function handleTabClick(index: number, key: string) {
    onTabIndexChange?.(index, key)
    autoScroll(index)
  }

  // Auto scroll to center the tab el
  function autoScroll(index: number) {
    if (typeof index !== 'number') return
    if (index === currentScrolledTabIndex) return

    const tabEl = tabEls[index]
    if (!tabEl || !containerRef) return

    const { marginLeft: tabMarginLeft } = getStyleAsInteger(tabEl, [
      'marginLeft',
    ])

    const halfScreen = document.documentElement.clientWidth / 2
    const halfTab = tabEl.offsetWidth / 2 + tabMarginLeft
    let newScrollLeft = tabEl.offsetLeft - halfScreen + halfTab

    if (DEBUG) {
      console.log(
        `[autoScroll] tabMarginLeft:${tabMarginLeft} / halfScreen:${halfScreen} / halfTab:${halfTab} / newScrollLeft:${newScrollLeft}`
      )
    }

    // keep most right element along the edge
    if (newScrollLeft > xMaxThreshold) {
      newScrollLeft = xMaxThreshold

      if (DEBUG) {
        console.log(`[autoScroll] keep most right element along the edge`)
      }
    }
    // keep most left element along the edge
    else if (newScrollLeft < 0) {
      newScrollLeft = 0

      if (DEBUG) {
        console.log(`[autoScroll] keep most left element along the edge`)
      }
    }

    // Scroll for tabs on the ends
    // or when delta is at least MINIMUM_MOVE px
    if (
      index === 0 ||
      index === data.length - 1 ||
      Math.abs(scrollLeft - newScrollLeft) > MINIMUM_MOVE
    ) {
      setScrollLeft(newScrollLeft)

      if (DEBUG) {
        console.log(`setScrollLeft ${newScrollLeft}`)
      }
    }
    setCurrentScrolledTabIndex(index)
  }

  function animeScroll(value: number) {
    anime({
      targets: containerRef.current,
      scrollLeft: value,
      easing: 'easeInOutQuad',
      duration: 600,
    })
  }
  if (DEBUG) {
    window.animeScroll = animeScroll
  }

  const tabRenderer = (
    {
      key,
      getTabContent,
      isCurrentTab,
      isClickable,
      ...remainingProps
    }: ITabData,
    index: number
  ) => (
    <Tab
      ref={(el) => setTabEl(el, index)}
      mode={mode}
      size={size}
      key={key}
      data-index={key}
      selected={isCurrentTab()}
      disabled={typeof isClickable === 'function' ? !isClickable() : false}
      onClick={() => {
        handleTabClick(index, key)
      }}
      {...remainingProps}
    >
      <TabContent>{getTabContent()}</TabContent>
    </Tab>
  )

  return (
    <ScrollableTabsContainer ref={containerRef}>
      <StyledTabs ref={tabsRef}>{data.map(tabRenderer)}</StyledTabs>
    </ScrollableTabsContainer>
  )
}

export default Tabs
