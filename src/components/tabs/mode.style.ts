import { TEXT_COLOR } from 'src/style.main'

export enum Mode {
  DARK = 'dark',
  LIGHT = 'light',
}
const DEFAULT_MODE = Mode.DARK

interface IModeStyle {
  color: string
  background: string
}

const MODE_STYLE: Record<Mode, { active: IModeStyle; inactive: IModeStyle }> = {
  [Mode.DARK]: {
    active: {
      color: TEXT_COLOR,
      background: '#0c073d',
    },
    inactive: {
      color: TEXT_COLOR,
      background: 'black',
    },
  },
  [Mode.LIGHT]: {
    active: {
      color: 'black',
      background: '#9cbeff',
    },
    inactive: {
      color: 'black',
      background: '#dddddd',
    },
  },
}

export const getModeStyle = (selected: boolean, mode?: Mode) =>
  MODE_STYLE[mode || DEFAULT_MODE][selected ? 'active' : 'inactive']
