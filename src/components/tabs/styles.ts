import styled from 'styled-components'

import ScrollbarsHidden from '../tweaks/ScrollbarsHidden'
import { FONT_SIZE } from 'src/style.main'
import { MOBILE_CSS_CLASS } from 'src/utils/Utils'
import { Size, getSizeStyle } from './size.styles'
import { Mode, getModeStyle } from './mode.style'

export const ScrollableTabsContainer = styled(ScrollbarsHidden)`
  flex-shrink: 0;
  max-width: 100vw;
  padding-top: 0.2rem;
  overflow-x: auto;
  overflow-y: hidden;
  -webkit-overflow-scrolling: touch;
`

export const StyledTabs = styled.div`
  display: flex;
  flex-grow: 1;
  width: max-content;
  margin: auto;
  padding: 0 0.3rem;
`

export const TabContent = styled.h2`
  display: flex;
  align-items: center;
  font-size: ${FONT_SIZE.smaller};
  margin: 0;
`

interface ITabProps {
  selected: boolean
  mode?: Mode
  size?: Size
  disabled: boolean
}

export const Tab = styled.span<ITabProps>`
  display: flex;
  align-items: center;
  padding: 0.5rem;
  max-width: 240px;
  font-style: normal;
  text-align: center;
  opacity: ${({ selected }) => (selected ? 1 : 0.8)};
  transition: opacity 0.6s;
  cursor: pointer;
  -webkit-tap-highlight-color: transparent;
  user-select: none;

  &:hover {
    opacity: 1;
  }

  & > *:only-child {
    width: 100%;
    border-radius: 3%;
    transition: background-color 0.6s;
    background-color: ${({ selected, mode }) =>
      getModeStyle(selected, mode).background};
  }

  & ${TabContent} {
    height: ${({ size }) => getSizeStyle(size).h2.height};
  }

  & a {
    display: inline-block;
    padding: ${({ size }) => getSizeStyle(size).a.padding};
    font-size: ${({ size }) => getSizeStyle(size).a.fontSize};
    text-decoration: none;
    color: ${({ selected, mode }) => getModeStyle(selected, mode).color};
    letter-spacing: ${({ size }) => getSizeStyle(size).a.letterSpacing};

    .${MOBILE_CSS_CLASS} & > * {
      margin-top: 0.2rem;
      margin-bottom: 0.2rem;
    }
  }
  & small {
    font-size: 0.75rem;
  }
`
