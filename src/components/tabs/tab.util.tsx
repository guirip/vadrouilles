import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import { Lang } from 'src/lang/Lang'
import { ITextPage } from 'src/types/Text'
import { ITabData } from '.'
import { SUBTITLE_COLOR } from 'src/style.main'

const StyledLink = styled(Link)`
  display: flex;
  flex-direction: column;
`
const Title = styled.div``
const Subtitle = styled.div`
  font-size: 0.75rem;
  color: ${SUBTITLE_COLOR};
`

const getPageI18nValue = (
  field: 'title' | 'subtitle',
  textPage: ITextPage,
  lang: Lang
) => textPage[field]?.[lang] ?? ''

export const generateTabsForPages = (
  textPages: ITextPage[],
  currentTab: string,
  baseRoute: string,
  lang: Lang
): ITabData[] =>
  textPages.map((textPage) => {
    const title = getPageI18nValue('title', textPage, lang)
    const subtitle = getPageI18nValue('subtitle', textPage, lang)
    return {
      key: textPage.slug,
      getTabContent: () => (
        <StyledLink to={baseRoute + '/' + textPage.slug}>
          <Title>{title}</Title>
          {subtitle && <Subtitle>{subtitle}</Subtitle>}
        </StyledLink>
      ),
      isCurrentTab: () => currentTab === textPage.slug,
    }
  })
