import React from 'react'
import styled from 'styled-components'

import { getCurrentLang } from 'src/lang'
import { Lang } from 'src/lang/Lang'
import { useLangContext } from 'src/context/LangContext'

const Flag = styled.div`
  flex-shrink: 0;
  width: 28px;
  height: 28px;
  margin-right: 2vw;
  opacity: 0.7;
  background-size: contain;
  cursor: pointer;
  background-image: ${(props) => `url(/images/flag-${props.lang}.png)`};
  -webkit-tap-highlight-color: transparent;
  transition: opacity 0.3s;

  &:hover {
    opacity: 0.9;
  }
`

function LangSwitch() {
  const { updateCurrentLang } = useLangContext()

  const nextLang = getCurrentLang() === Lang.fr ? Lang.en : Lang.fr

  function handleClick() {
    updateCurrentLang(nextLang)
  }

  return <Flag onClick={handleClick} lang={nextLang} />
}

export default LangSwitch
