import React, { PropsWithChildren } from 'react'
import styled from 'styled-components'
import { Fade } from '../fade'

export const Container = styled.div`
  overflow-x: hidden;

  @media (min-width: 768px) {
    & {
      margin-right: 8vw;
      margin-left: 8vw;
    }
  }
  @media (min-width: 1024px) {
    & {
      margin-right: 6vw;
      margin-left: 6vw;
    }
  }
`

export const CoreContent = ({ children }: PropsWithChildren) => (
  <Container>
    <Fade>{children}</Fade>
  </Container>
)
