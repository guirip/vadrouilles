import React from 'react'
import styled from 'styled-components'
import { Anchor, SocialName } from './common.style'

import config from 'src/config'
import { Category } from 'src/types/Category'

const YoutubeIcon = styled.i`
  font-size: 1.3rem;
`

interface IProps {
  category: Category
}

export const YoutubeLink = ({ category }: IProps) =>
  config.MY_YOUTUBE[category].url ? (
    <Anchor href={config.MY_YOUTUBE[category].url}>
      <YoutubeIcon className="pi pi-youtube" />
      <SocialName>{config.MY_YOUTUBE[category].name}</SocialName>
    </Anchor>
  ) : null
