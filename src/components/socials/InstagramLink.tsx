import React from 'react'
import styled from 'styled-components'
import { Anchor, SocialName } from './common.style'

import config from 'src/config'
import { Category } from 'src/types/Category'

const InstagramIcon = styled.i`
  font-size: 1.1rem;
`

interface IProps {
  category: Category
}

export const InstagramLink = ({ category }: IProps) =>
  config.MY_INSTAGRAM[category].url ? (
    <Anchor href={config.MY_INSTAGRAM[category].url}>
      <InstagramIcon className="pi pi-instagram" />
      <SocialName>{config.MY_INSTAGRAM[category].name}</SocialName>
    </Anchor>
  ) : null
