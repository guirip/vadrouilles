import styled from 'styled-components'
import { TEXT_COLOR } from 'src/style.main'
import { StyledHeader } from '../header/styles'
import { MOBILE_CSS_CLASS } from 'src/utils/Utils'

export const Anchor = styled.a`
  display: flex;
  margin: 0.3rem 1rem;
  padding: 0.5em 0;
  align-items: center;
  color: ${TEXT_COLOR};
  text-decoration: none;
  transition: opacity 0.4s;

  ${StyledHeader} & {
    opacity: 0.6;
    &:hover {
      opacity: 1;
    }
    & i {
      font-size: 1.2rem;
    }
  }

  .${MOBILE_CSS_CLASS} & {
    margin-right: 2vw;
    margin-left: 2vw;
  }
`

export const SocialName = styled.span`
  margin-left: 8px;
  font-size: 0.8rem;
  font-weight: bold;

  &:hover {
    text-decoration: underline;
  }

  .${MOBILE_CSS_CLASS} ${StyledHeader} & {
    display: none;
  }
`
