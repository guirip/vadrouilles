import { useEffect, useState } from 'react'

import config from 'src/config'
import { ICatalogItemCategory, IItemsPerCategory } from 'src/types/CatalogItem'
import { executeRequest } from 'src/utils/Utils'
import { ITabData } from '../tabs'
import { generateCategoryTabContent } from './catalog.util'

interface IResponse {
  itemsPerCategory: IItemsPerCategory
  categories: ICatalogItemCategory[]
}

function computeTabData(
  itemsPerCategory: IItemsPerCategory,
  categories: ICatalogItemCategory[],
  isCurrentTab: (catId: ICatalogItemCategory['_id']) => boolean
) {
  const tabData: ITabData[] = []

  for (const cat of categories) {
    if (itemsPerCategory[cat._id]) {
      tabData.push({
        key: cat.slug,
        getTabContent: () => generateCategoryTabContent(cat),
        isCurrentTab: () => isCurrentTab(cat._id),
      })
    }
  }
  return tabData
}

export function useCatalog(
  currentCatId: ICatalogItemCategory['_id'] | undefined
) {
  const [error, setError] = useState<unknown>()
  const [categories, setCategories] = useState<ICatalogItemCategory[]>([])
  const [itemsPerCategory, setItemsPerCategory] = useState<IItemsPerCategory>(
    {}
  )
  const [tabData, setTabData] = useState<ITabData[]>([])

  async function fetchItems() {
    let response, error
    try {
      const result = await executeRequest(
        `${config.BACKEND_URL}${config.GET_CATALOG_PATH}`
      )
      response = result.response
      error = result.error
    } catch (e) {
      error = e
    }

    if (response && response.ok) {
      try {
        const { itemsPerCategory, categories } =
          (await response.json()) as IResponse
        setCategories(categories ?? [])
        setItemsPerCategory(itemsPerCategory || {})
      } catch (e) {
        error = e
      }
    }
    setError(error)
  }

  useEffect(() => {
    fetchItems()
  }, [])

  useEffect(() => {
    setTabData(
      computeTabData(
        itemsPerCategory,
        categories,
        (catId) => catId === currentCatId
      )
    )
  }, [itemsPerCategory, categories, currentCatId])

  return {
    error,
    tabData,
    categories,
    itemsPerCategory,
  }
}
