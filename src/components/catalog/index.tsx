import React, { useEffect, useMemo, useState } from 'react'
import { Helmet } from 'react-helmet'
import { Fieldset } from 'primereact/fieldset'

import { useLangContext } from 'src/context/LangContext'
import { useCodifContext } from 'src/context/CodifContext'

import config from 'src/config'
import { CoreContent } from 'src/components/core-content'
import Tabs from 'src/components/tabs'
import { useCatalog } from './catalog.hook'
import { findCategoryBySlug, getNoticeContent } from './catalog.util'
import { Mode } from '../tabs/mode.style'
import { Size } from '../tabs/size.styles'
import CatalogItem from 'src/components/catalog-item'
import { ICatalogItemCategory } from 'src/types/CatalogItem'

import {
  Notice,
  UsageWrapped,
  UsageSteps,
  UsageLastLine,
  Usage,
  Links,
  StyledCatalog,
} from './Catalog.style'

import './Catalog.css'

interface IProps {
  categorySlug?: ICatalogItemCategory['slug']
}

function Catalog({ categorySlug }: IProps) {
  const { labels } = useLangContext()

  const { catalogNotice } = useCodifContext()
  const noticeContent = getNoticeContent(catalogNotice)

  const [currentCategoryId, setCurrentCategoryId] = useState<
    ICatalogItemCategory['_id'] | undefined
  >()

  const { error, tabData, itemsPerCategory, categories } =
    useCatalog(currentCategoryId)

  const fallbackCategory = useMemo(
    () => (Array.isArray(categories) ? categories[0] : undefined),
    [categories]
  )

  const initialCategoryId = useMemo(() => {
    let slug: string | undefined
    if (categorySlug) {
      slug = findCategoryBySlug(categorySlug, categories)?._id
      if (slug) {
        return slug
      }
    }
    if (fallbackCategory) {
      return fallbackCategory._id
    }
  }, [categorySlug, fallbackCategory, categories])

  useEffect(() => {
    if (currentCategoryId !== initialCategoryId) {
      setCurrentCategoryId(initialCategoryId)
    }
  }, [currentCategoryId, initialCategoryId])

  function onTabChange(_index: number, catSlug: string) {
    const cat = findCategoryBySlug(catSlug, categories)
    setCurrentCategoryId(cat ? cat._id : fallbackCategory?._id)
  }

  return (
    <div>
      <Helmet>
        <title>{`${labels.catalog.pageTitle} ${labels.appTitle}`}</title>
        <meta name="description" content={labels.catalog.pageDescription} />
      </Helmet>

      <CoreContent>
        <>
          {error && <div>{`${labels.catalog.fetchError} ${error}`}</div>}
          {noticeContent && (
            <div className="mx-2">
              <Notice dangerouslySetInnerHTML={{ __html: noticeContent }} />
            </div>
          )}

          <div className="mx-2 mt-3">
            <Fieldset legend={labels.catalog.usageTitle} toggleable collapsed>
              <UsageWrapped>
                {/* <div><b>{labels.catalog.usageTitle}</b></div> */}
                {labels.catalog.usageFirstLine && (
                  <div>{labels.catalog.usageFirstLine}</div>
                )}
                <UsageSteps
                  dangerouslySetInnerHTML={{
                    __html: labels.catalog.usageSteps,
                  }}
                />
                {labels.catalog.usageLastLine && (
                  <UsageLastLine>{labels.catalog.usageLastLine}</UsageLastLine>
                )}
              </UsageWrapped>
            </Fieldset>

            <Usage>
              {labels.catalog.contactLine && (
                <div>{labels.catalog.contactLine}</div>
              )}
              <Links>
                <div>
                  <i className="fas fa-envelope" />
                  <a href={`mailto:${config.MY_EMAIL}`}>{config.MY_EMAIL}</a>
                </div>
                <div>
                  <i className="fab fa-instagram" />
                  <a href={config.MY_INSTAGRAM.graffiti.url}>
                    {config.MY_INSTAGRAM.graffiti.name}
                  </a>
                </div>
              </Links>
            </Usage>
          </div>

          {tabData.length > 1 && (
            <Tabs
              mode={Mode.LIGHT}
              size={Size.SMALLER}
              data={tabData}
              onTabIndexChange={onTabChange}
            />
          )}

          {currentCategoryId &&
            itemsPerCategory[currentCategoryId] &&
            itemsPerCategory[currentCategoryId].length > 0 && (
              <StyledCatalog>
                {itemsPerCategory[currentCategoryId].map((item) => (
                  <CatalogItem key={item._id} item={item} />
                ))}
              </StyledCatalog>
            )}
        </>
      </CoreContent>
    </div>
  )
}

export default Catalog
