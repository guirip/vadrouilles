import React from 'react'
import { Link } from 'react-router-dom'
import { getCurrentLang } from 'src/lang'
import { Lang } from 'src/lang/Lang'
import Tab from 'src/routes/graffiti/GraffitiTab'
import Route from 'src/routes/Route'
import { ICatalogItemCategory } from 'src/types/CatalogItem'
import { IStringCodif } from 'src/types/Codif'
import { CategoryTabContent } from './Catalog.style'

export function getNoticeContent(noticeValue: IStringCodif['value'] | null) {
  if (!noticeValue) {
    return null
  }
  try {
    // Test if notice containes i18n properties (such as 'fr', 'en')
    const noticeContent = JSON.parse(noticeValue) as Record<Lang, string>
    if (noticeContent[getCurrentLang()]) {
      return noticeContent[getCurrentLang()]
    }
    if (typeof noticeContent !== 'string') {
      return null
    }
  } catch (e) {
    // Case where value is a simple string
    return noticeValue
  }
}

export function findCategoryBySlug(
  catSlug: ICatalogItemCategory['slug'],
  categories: ICatalogItemCategory[]
) {
  return categories.find((cat) => cat.slug === catSlug)
}

export const generateCategoryTabContent = (category: ICatalogItemCategory) => (
  <Link to={`${Route.GRAFFITI}/${Tab.CATALOG}/${category.slug}`}>
    <CategoryTabContent>{category.name[getCurrentLang()]}</CategoryTabContent>
  </Link>
)
