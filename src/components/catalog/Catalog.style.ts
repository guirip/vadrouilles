import { FONT_SIZE } from 'src/style.main'
import styled from 'styled-components'

export const StyledCatalog = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: flex-start;
  flex-wrap: wrap;
  margin: 1rem 0.5rem;

  @media (max-width: 760px) {
    flex-direction: column;
    align-items: center;
  }
`

export const Notice = styled.div`
  margin: 1rem auto 0.5rem;
  padding: 0.4rem 1rem;
  width: fit-content;
  font-style: normal;
  font-size: ${FONT_SIZE.content};
  text-align: center;
  font-weight: bold;
  line-height: 1.5em;
  background-color: rgba(255, 255, 255, 0.1);
`

export const UsageWrapped = styled(Notice)`
  font-size: ${FONT_SIZE.content};
  font-weight: normal;
  text-align: left;
  background-color: transparent;
`

export const Usage = styled(UsageWrapped)`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 1rem 1.5rem;
  background-color: rgba(0, 0, 0, 0.45);

  b {
    display: block;
    font-size: ${FONT_SIZE.content};
    margin-bottom: 0.5rem;
    text-align: center;
  }
`

export const UsageSteps = styled.ul`
  margin: auto;
  width: fit-content;

  & li {
    margin: 0.5rem 0;
  }

  @media (max-width: 576px) {
    & {
      padding-left: 20px;
    }
  }
`

export const UsageLastLine = styled.div`
  line-height: 1.8em;
`

export const Links = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 0.5rem;
  flex-wrap: wrap;
  font-size: ${FONT_SIZE.content};
  letter-spacing: 1px;

  & > div {
    display: flex;
    align-items: center;
    margin: 0.2rem 1rem;
  }
  & i {
    margin: 0 0.5rem;
    font-size: 1.8rem;
  }
  & a {
    color: #a0e4ff;
  }
`

export const ItemsCount = styled.div`
  text-align: center;
  font-style: normal;
  font-size: ${FONT_SIZE.content};
`

export const CategoryTabContent = styled.h4`
  color: black;
  font-weight: bold;
  padding: 0.3rem 0.8rem;
`
