import React from 'react'
import styled, { CSSProperties } from 'styled-components'

const StyledBigButton = styled.div`
  width: 100%;
  height: 100%;
  background-repeat: no-repeat;
  background-size: cover;
  cursor: pointer;
  box-shadow: 0px 0px 15px 5px rgba(0, 0, 0, 0.3);
`
/*
const BigButtonText = styled.div`
  display: flex;
  height: 100%;
  justify-content: center;
  align-items: center;
  font-size: 3.4em;
  -webkit-text-stroke: 3px black;
`
*/

interface IProps {
  image: string
  text: string
  backgroundPosition: string
}

function BigButton({ image, backgroundPosition }: IProps) {
  const style: CSSProperties = {
    backgroundImage: 'url(' + image + ')',
  }
  if (backgroundPosition) {
    style.backgroundPosition = backgroundPosition
  }

  return (
    <StyledBigButton style={style}>
      {/* text && <BigButtonText>{text}</BigButtonText> */}
    </StyledBigButton>
  )
}

export default BigButton
