import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { InstagramLink } from '../socials/InstagramLink'
import { YoutubeLink } from '../socials/YoutubeLink'
import { Category } from 'src/types/Category'
import { Anchor } from '../socials/common.style'

interface IStyledFooterProps {
  isVisible: boolean
}

export const StyledFooter = styled.div<IStyledFooterProps>`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 1.7em 0 1rem;
  padding: 0.5rem;
  background-color: rgba(0, 0, 0, 0.4);
  width: 100%;
  opacity: ${(props) => (props.isVisible ? 1 : 0)};
  transition: opacity 0.5s;

  & ${Anchor} {
    margin: 0.3rem 2rem;
    opacity: 0.75;

    &:hover {
      opacity: 1;
    }
  }
`

interface IProps {
  category: Category
}

export function Footer({ category }: IProps) {
  const [isVisible, setVisibility] = useState(false)

  useEffect(() => {
    const timer = window.setTimeout(setVisibility, 1500, true)

    return function () {
      // Avoid updating the component state when it is not mounted anymore
      // (e.g switching page during the 1500ms delay)
      window.clearTimeout(timer)
    }
  }, [isVisible])

  return (
    <StyledFooter isVisible={isVisible}>
      <InstagramLink category={category} />
      <YoutubeLink category={category} />
    </StyledFooter>
  )
}
