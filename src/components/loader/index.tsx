import React from 'react'
import styled from 'styled-components'

import { useLangContext } from 'src/context/LangContext'
import { Fade } from '../fade'
import { FontFamily } from 'src/fonts'

export enum LoaderSize {
  SMALL = 'small',
  MEDIUM = 'medium',
  BIG = 'big',
}

const sizeToScale = {
  [LoaderSize.SMALL]: 0.3,
  [LoaderSize.MEDIUM]: 0.6,
  [LoaderSize.BIG]: 1,
}

interface IWrapperProps {
  size: LoaderSize
}

const Wrapper = styled.div<IWrapperProps>`
  display: flex;
  width: 100%;
  height: 80%;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  transform: ${({ size }) => `scale(${sizeToScale[size]})`};
`
const LoaderIcon = styled.i`
  font-size: 7em;
  color: lightgrey;
`
const LoaderText = styled.div`
  margin: 1em;
  font-size: 2em;
  font-family: ${FontFamily.MONTSERRAT}, sans-serif;
  color: lightgrey;
`

interface IProps {
  size?: LoaderSize
  fadeDuration?: number
}

function Loader({ size = LoaderSize.BIG, fadeDuration }: IProps) {
  const { labels } = useLangContext()

  return (
    <Fade duration={fadeDuration}>
      <Wrapper size={size}>
        <LoaderIcon className="fas fa-clock" />
        <LoaderText>{labels.loading}</LoaderText>
      </Wrapper>
    </Fade>
  )
}

export default Loader
