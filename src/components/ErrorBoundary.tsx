import React, { Component, PropsWithChildren } from 'react'
import styled from 'styled-components'

import LangContext from 'src/context/LangContext'
import { FONT_SIZE } from 'src/style.main'

const Message = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 4em;
  color: white;
  font-size: ${FONT_SIZE.h2};
`

class ErrorBoundary extends Component {
  state = {
    hasError: false,
  }

  static getDerivedStateFromError() {
    return {
      hasError: true,
    }
  }

  componentDidCatch(error: unknown, errorInfo: unknown) {
    console.error('did catch error: ', error, errorInfo)
  }

  render() {
    if (this.state.hasError) {
      return (
        <LangContext.Consumer>
          {({ labels }) => <Message>{labels.error}</Message>}
        </LangContext.Consumer>
      )
    }

    return (this.props as PropsWithChildren).children
  }
}

export default ErrorBoundary
