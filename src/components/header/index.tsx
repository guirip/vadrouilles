import React, { useMemo } from 'react'
import styled from 'styled-components'

import Route from 'src/routes/Route'
import { useLangContext } from 'src/context/LangContext'
import LangSwitch from 'src/components/lang-switch'
import {
  StyledHeader,
  SideButtonContainer,
  HeaderTitle,
  StyledLink,
} from './styles'
import { InstagramLink } from '../socials/InstagramLink'
import { YoutubeLink } from '../socials/YoutubeLink'
import { Category } from 'src/types/Category'
import { IAppLabels } from 'src/types/AppLabels'

const CategoryName = styled.span`
  font-size: 0.8rem;
`

const SHOW_CATEGORY_NAME = true

interface IProps {
  currentRoute: string
  category: Category
}

function getLabel(labels: IAppLabels, route: string) {
  switch (route) {
    case Route.GRAFFITI:
    case Route.SEULS_NOS_CORPS:
      return labels.graffiti.toUpperCase()

    case Route.DEFAULT:
    case Route.NATURE:
      return labels.nature.toUpperCase()

    case Route.TEXT_PREVIEW:
      return labels.text.preview.toUpperCase()

    case Route.ABOUT:
      return labels.about.toUpperCase()

    default:
      return ''
  }
}

function Header({ currentRoute, category }: IProps) {
  const { labels } = useLangContext()

  const label = useMemo(
    () => getLabel(labels, currentRoute),
    [labels, currentRoute]
  )

  return (
    <StyledHeader>
      <SideButtonContainer left>
        <LangSwitch />
        <InstagramLink category={category} />
        <YoutubeLink category={category} />
      </SideButtonContainer>

      <HeaderTitle
        category={category}
        dangerouslySetInnerHTML={{
          __html: label ?? '',
        }}
      />

      <SideButtonContainer left={false}>
        {currentRoute !== Route.GRAFFITI && (
          <StyledLink to={Route.GRAFFITI} title={labels.graffiti}>
            <i className="fas fa-paint-brush" />
            {SHOW_CATEGORY_NAME && (
              <CategoryName>{labels.graffiti_short}</CategoryName>
            )}
          </StyledLink>
        )}

        {currentRoute !== Route.NATURE && (
          <StyledLink to={Route.NATURE} title={labels.nature}>
            <i className="fas fa-tree" />
            {SHOW_CATEGORY_NAME && (
              <CategoryName>{labels.nature_short}</CategoryName>
            )}
          </StyledLink>
        )}

        {currentRoute !== Route.ABOUT && (
          <StyledLink to={Route.ABOUT} title={labels.about}>
            <i className="fas fa-smile-beam" />
            {SHOW_CATEGORY_NAME && (
              <CategoryName>{labels.about_short}</CategoryName>
            )}
          </StyledLink>
        )}
      </SideButtonContainer>
    </StyledHeader>
  )
}

export default Header
