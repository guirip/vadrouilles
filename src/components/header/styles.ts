import { Link } from 'react-router-dom'
import { FontFamily } from 'src/fonts'
import { FONT_SIZE, TEXT_COLOR } from 'src/style.main'
import { Category } from 'src/types/Category'
import { MOBILE_CSS_CLASS } from 'src/utils/Utils'
import styled from 'styled-components'

export const StyledHeader = styled.div`
  flex-shrink: 0;
  display: flex;
  align-items: center;
  margin: .3em 0 0em;
  padding: .5em;
  width: 100%;
  background-color: rgba(0,0,0,.4);

  // @media (orientation: landscape) and (max-width: 1000px) {
  .${MOBILE_CSS_CLASS} & {
    padding .2rem;
    font-size: ${FONT_SIZE.smallest};

    & h1 {
      margin: 0;
    }
  }
`

interface ISideButtonContainerProps {
  left: boolean
}
export const SideButtonContainer = styled.div<ISideButtonContainerProps>`
  display: flex;
  align-items: center;
  padding: 0 0.4em;
  flex: 0 0 30%;
  justify-content: ${({ left }) => (left ? 'flex-start' : 'flex-end')};
  flex-wrap: ${({ left }) => (left ? 'nowrap' : 'wrap')};
`

interface IHeaderTitleProps {
  category: Category
}

export const HeaderTitle = styled.h2<IHeaderTitleProps>`
  display: flex;
  flex-grow: 1;
  justify-content: center;
  margin: 5px 0.5em 0;
  line-height: 22px;
  font-family: ${({ category }) =>
    category === Category.GRAFFITI
      ? FontFamily.LUCKIESTGUY
      : `${FontFamily.SALSA} !important`};
  font-size: ${({ category }) =>
    category === Category.GRAFFITI ? FONT_SIZE.h4 : FONT_SIZE.h6};
  font-style: normal;
  font-weight: normal;
  font-family: ${FontFamily.LUCKIESTGUY};
  text-align: center;
  letter-spacing: 4px;
  color: #c3c2c2;

  .${MOBILE_CSS_CLASS} & {
    font-size: 15px;
  }
`

export const StyledLink = styled(Link)`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: 0.5rem;
  padding: 0.4em 0.6rem;
  font-size: ${FONT_SIZE.h3};
  background-color: rgba(0, 0, 0, 0.5);
  border-radius: 5px;
  border: 1px solid black;
  cursor: pointer;
  text-decoration: none;
  color: ${TEXT_COLOR};
  transition: opacity 0.4s;
  opacity: 0.7;

  &:hover {
    opacity: 1;
  }

  & i {
    font-size: 1.1rem;
  }

  & span {
    margin: 0.1rem 0.2rem 0 0.4rem;
    font-weight: bold;
    white-space: nowrap;
  }

  .${MOBILE_CSS_CLASS} & {
    margin: 0.1rem 0;
    padding: 0.2em 0.6rem;
  }
`
