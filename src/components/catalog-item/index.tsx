import React from 'react'

import { useLangContext } from 'src/context/LangContext'
import { ICatalogItem } from 'src/types/CatalogItem'
import { useCodifContext } from 'src/context/CodifContext'
import Gallery from '../gallery'
import {
  Container,
  ItemTitle,
  StatusPriceContainer,
  FieldStatus,
  InlineField,
  FieldName,
  Price,
  Field,
  CenteredText,
  FieldNameCentered,
} from './styles'
import { GallerySize } from '../gallery/types'

const getDiscountedPrice = (price: number, discount: number) =>
  Math.floor(price - (price * discount) / 100)

interface IProps {
  item: ICatalogItem
}

function CatalogItem({ item }: IProps) {
  const { labels } = useLangContext()
  const pageLabels = labels.catalog

  const { catalogDiscount } = useCodifContext()
  const hasDiscount = typeof catalogDiscount === 'number' && catalogDiscount > 0

  return (
    <Container status={item.status}>
      <Gallery
        visuals={item.files}
        size={GallerySize.SMALL}
        showGalleryThumbnails
      />

      <ItemTitle>{item.title}</ItemTitle>

      <StatusPriceContainer className="flex flex-row justify-content-evenly align-items-baseline">
        <FieldStatus className={item.status}>
          {pageLabels.status[item.status]}
        </FieldStatus>

        {item.price && item.status === 'available' && (
          <InlineField>
            <FieldName>{pageLabels.price}</FieldName>
            <Price isOldPrice={hasDiscount}>{item.price} €</Price>
            {hasDiscount && (
              <Price isNewPrice>
                {getDiscountedPrice(item.price, catalogDiscount)} €
              </Price>
            )}
          </InlineField>
        )}
      </StatusPriceContainer>

      <div>
        {item.description && (
          <Field>
            <CenteredText
              dangerouslySetInnerHTML={{ __html: item.description }}
            />
          </Field>
        )}

        {item.technique && (
          <Field>
            <FieldNameCentered>{pageLabels.technique}</FieldNameCentered>
            <CenteredText>{item.technique}</CenteredText>
          </Field>
        )}

        {item.dimensions && (
          <Field className="justify-content-center">
            <FieldNameCentered>{pageLabels.dimensions}</FieldNameCentered>
            <CenteredText>{item.dimensions}</CenteredText>
          </Field>
        )}
        {item.weight && (
          <InlineField className="justify-content-center">
            <FieldName>{pageLabels.weight}</FieldName>
            <CenteredText>{item.weight}</CenteredText>
          </InlineField>
        )}
      </div>
    </Container>
  )
}

export default CatalogItem
