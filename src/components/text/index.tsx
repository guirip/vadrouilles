import React, { useEffect, useMemo, useRef, useState } from 'react'
import styled from 'styled-components'
import { Button } from 'primereact/button'
import { Helmet } from 'react-helmet'
import { Link } from 'react-router-dom'
import { Toast } from 'primereact/toast'

import { useLangContext } from 'src/context/LangContext'
import { Lang, SUPPORTED_LANG } from 'src/lang/Lang'
import ErrorMessage from 'src/components/ErrorMessage'
import Tabs, { ITabData } from '../tabs'
import { Mode } from '../tabs/mode.style'
import { Size } from '../tabs/size.styles'
import LocalizedText from './LocalizedText'
import { TextWrapper } from './text-components/TextWrapper'
// import CommentsBlock from './CommentsBlock'
// import { ScrollableContent } from 'src/routes/pageStyle'

import config from 'src/config'
import {
  ILocalizedText,
  ITextChapterPopulated,
  ITextPagePopulated,
} from 'src/types/Text'
import { useFetch } from 'src/utils/useFetch'
import Loader, { LoaderSize } from '../loader'
import { CoreContent } from '../core-content'
import { IAppLabels } from 'src/types/AppLabels'
import { getErrorMessage } from 'src/utils/Utils'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`
const TabContent = styled.h4``
const LoaderWrapper = styled.div`
  margin-top: 4vh;
  margin-bottom: 90vh;
`
const StyledLink = styled(Link)`
  text-decoration: none;
`

const findTextByLang = (texts: ILocalizedText[], lang: Lang) =>
  texts.find((t) => t.lang === lang)

function getLocalizedTextByLang(
  chapter: ITextChapterPopulated,
  currentLang: Lang
) {
  let localizedText = findTextByLang(chapter.localizedTexts, currentLang)
  if (localizedText) {
    return localizedText
  }
  for (const lang of SUPPORTED_LANG.filter((l) => l !== currentLang)) {
    localizedText = findTextByLang(chapter.localizedTexts, lang)
    if (localizedText) {
      return localizedText
    }
  }
}

const getLocalizedTextTitleByLang = (
  chapter: ITextChapterPopulated,
  labels: IAppLabels
) => getLocalizedTextByLang(chapter, labels.lang)?.title || labels.text.untitled

function getInitialIndex(
  text: ITextPagePopulated,
  chapterSlug?: ITextChapterPopulated['slug']
) {
  if (!text) {
    return -1
  }
  if (chapterSlug) {
    const chapterIndex = text?.chapters?.findIndex(
      (c) => c.slug === chapterSlug
    )
    if (chapterIndex !== -1) {
      return chapterIndex
    }
  }
  return 0
}

const getChapterLink = (
  textPath: string,
  chapterSlug: ITextChapterPopulated['slug']
) => `${textPath}/${chapterSlug}`

interface IProps {
  text: ITextPagePopulated
  textPath: string
  chapterSlug?: ITextChapterPopulated['slug']
  resetScroll: () => void
}

function Text({ text, textPath, chapterSlug, resetScroll }: IProps) {
  const toastRef = useRef<Toast>(null)
  const { labels } = useLangContext()
  const [nextChapterSlug, setNextChapterSlug] = useState<string | null>(null)
  const [nextChapterTitle, setNextChapterTitle] = useState<string | null>(null)
  // const [commentBlockVisible, setCommentBlockVisible] = useState(false)

  const [currentChapterIndex, setCurrentChapterIndex] = useState(
    getInitialIndex(text, chapterSlug)
  )
  useEffect(() => {
    setCurrentChapterIndex(getInitialIndex(text, chapterSlug))
  }, [setCurrentChapterIndex, text, chapterSlug])

  function onTabChange(newIndex: number) {
    setCurrentChapterIndex(newIndex)
  }

  const currentChapter = useMemo(
    () =>
      text && currentChapterIndex !== -1
        ? text.chapters[currentChapterIndex]
        : undefined,
    [text, currentChapterIndex]
  )

  const fetchParams = useMemo(
    () => ({
      chapterSlug: currentChapter?.slug,
      lang: labels.lang,
    }),
    [labels.lang, currentChapter?.slug]
  )

  const [loading, fetchLocalizedText, error, localizedText] =
    useFetch<ILocalizedText>(
      config.GET_LOCALIZED_TEXT_PATH,
      undefined,
      fetchParams
    )

  useEffect(() => {
    if (
      currentChapterIndex !== -1 &&
      currentChapterIndex < text.chapters.length - 1
    ) {
      const nextChapter = text.chapters[currentChapterIndex + 1]
      setNextChapterSlug(nextChapter.slug)
      setNextChapterTitle(getLocalizedTextTitleByLang(nextChapter, labels))
    } else {
      setNextChapterSlug(null)
      setNextChapterTitle(null)
    }
  }, [currentChapterIndex, labels, text.chapters])

  /*
  useEffect(() => {
    setCommentBlockVisible(false);
  }, [currentChapterIndex])
  */

  useEffect(() => {
    resetScroll()
  }, [currentChapterIndex, resetScroll])

  useEffect(() => {
    if (currentChapterIndex === -1) {
      return
    }
    if (!currentChapter) {
      if (localizedText) {
        console.error('No chapter found for index ' + currentChapterIndex)
      }
    } else {
      const _localizedText = getLocalizedTextByLang(
        text.chapters[currentChapterIndex],
        labels.lang
      )
      if (!_localizedText) {
        // showNotification(toastRef.current, '', '', ToastMessage)
        return
      }
      if (!localizedText || localizedText._id !== _localizedText._id) {
        fetchLocalizedText()
      }
    }
  }, [
    text,
    currentChapter,
    currentChapterIndex,
    labels.lang,
    fetchLocalizedText,
    localizedText,
  ])

  const tabData: ITabData[] = useMemo(
    function getTabData() {
      if (!text || text.chapters.length < 2) {
        return []
      }
      return text.chapters.map(function (c, index) {
        const tabLabel = getLocalizedTextTitleByLang(c, labels)
        return {
          key: c.slug,
          label: tabLabel,
          getTabContent: () => (
            <Link to={getChapterLink(textPath, c.slug)}>
              <TabContent>{tabLabel}</TabContent>
            </Link>
          ),
          isCurrentTab: () => index === currentChapterIndex,
        }
      })
    },
    [text, textPath, currentChapterIndex, labels]
  )

  if (!text) {
    return null
  }
  if (!text.chapters || text.chapters.length === 0) {
    return null
  }

  let description = text.description
  if (localizedText) {
    description += ' - ' + localizedText.description
  }

  return (
    <Wrapper>
      <>
        <Helmet>
          <title>{`${text.title[labels.lang]} ⛺ ${labels.appTitle}`}</title>
          <meta name="description" content={description} />
        </Helmet>

        <Toast ref={toastRef} />

        <Tabs
          mode={Mode.LIGHT}
          size={Size.SMALLER}
          data={tabData}
          onTabIndexChange={onTabChange}
        />

        {error && (
          <ErrorMessage
            text={<TextWrapper text={labels.text.fetchError} />}
            errMessage={getErrorMessage(error)}
          />
        )}
        {loading && (
          <LoaderWrapper>
            <Loader size={LoaderSize.SMALL} fadeDuration={3000} />
          </LoaderWrapper>
        )}
        {!loading && !error && (
          <CoreContent>
            <LocalizedText item={localizedText} />
          </CoreContent>
        )}

        {nextChapterSlug && nextChapterTitle && (
          <div className="flex justify-content-end">
            <StyledLink to={getChapterLink(textPath, nextChapterSlug)}>
              <Button
                className="p-button-sm mt-2 mx-5"
                icon="pi pi-arrow-right"
                iconPos="right"
                label={nextChapterTitle}
              />
            </StyledLink>
          </div>
        )}
        {/*
        <CommentsBlock
          visible={commentBlockVisible}
          setBlockVisible={() => setCommentBlockVisible(true)}
          identifier={text.slug+'@'+getCurrentChapter().slug}
          title={tabData[currentChapterIndex].label}
        />
        */}
      </>
    </Wrapper>
  )
}

export default Text
