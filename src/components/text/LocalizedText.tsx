import React, { createRef } from 'react'
import styled from 'styled-components'
import { useLangContext } from 'src/context/LangContext'
import { ILocalizedText } from 'src/types/Text'
import { TextWrapper } from './text-components/TextWrapper'
import { Block } from './text-components/Block'
import { LocalizedTextWrapper } from './LocalizedTextWrapper'

const NoContent = styled.div`
  padding: 4rem 2rem 7rem;
  text-align: center;
`
const LangNotAvailable = styled.div`
  text-align: center;
`
interface IProps {
  item: ILocalizedText | null
}

function LocalizedText({ item }: IProps) {
  const { labels } = useLangContext()
  const containerRef = createRef<HTMLDivElement>()

  if (
    !item ||
    !item.content ||
    Array.isArray(item.content.blocks) !== true ||
    item.content.blocks.length === 0
  ) {
    return (
      <NoContent>
        <TextWrapper text={labels.noContent} />
      </NoContent>
    )
  }

  return (
    <LocalizedTextWrapper ref={containerRef} className="px-2 py-5">
      {item.lang !== labels.lang && (
        <LangNotAvailable>{labels.langNotAvailable}</LangNotAvailable>
      )}
      {item.content.blocks.map((block, index) => (
        <Block
          key={block.id ?? index}
          block={block}
          containerRef={containerRef}
        />
      ))}
    </LocalizedTextWrapper>
  )
}

export default LocalizedText
