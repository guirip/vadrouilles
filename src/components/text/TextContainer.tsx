import React, { useEffect, useMemo } from 'react'
import config from 'src/config'
import { useLangContext } from 'src/context/LangContext'
import { ITextChapter, ITextPagePopulated } from 'src/types/Text'
import { useFetch } from 'src/utils/useFetch'
import ErrorMessage from '../ErrorMessage'
import Loader, { LoaderSize } from '../loader'
import Text from './index'

interface IProps {
  textSlug: ITextPagePopulated['slug']
  chapterSlug?: ITextChapter['slug']
  baseRoute: string
  resetScroll: () => void
  visibility: 'all' | null
}

export const TextContainer = ({
  textSlug,
  chapterSlug,
  baseRoute,
  resetScroll,
  visibility,
}: IProps) => {
  const { labels } = useLangContext()

  const fetchParams = useMemo(
    () => ({
      _slug: textSlug,
      visibility,
    }),
    [textSlug, visibility]
  )

  const [loading, fetchTextPage, error, textPage] =
    useFetch<ITextPagePopulated>(config.GET_TEXT_PATH, undefined, fetchParams)

  useEffect(() => {
    fetchTextPage()
  }, [fetchTextPage])

  if (loading) {
    return <Loader size={LoaderSize.SMALL} fadeDuration={500} />
  }
  if (error || !textPage) {
    return <ErrorMessage text={labels.text.fetchError} errMessage={error} />
  }
  return (
    <Text
      text={textPage}
      textPath={`${baseRoute}/${textSlug}`}
      chapterSlug={chapterSlug}
      resetScroll={resetScroll}
    />
  )
}
