import React from 'react'
import styled from 'styled-components'
import { IListBlock, INestedListItem } from 'src/types/Text'
import { TextWrapper } from './TextWrapper'
import { MAX_WIDTH } from './constants'

const MARGIN_X = 20

const Wrapper = styled.div`
  margin: 0 ${MARGIN_X}px;
  max-width: ${MAX_WIDTH - MARGIN_X * 2}px;
  padding: 0.1rem 0.6rem 0.1rem 0;
  background-color: rgba(0, 0, 0, 0.25);

  & ul,
  & ol {
  }
`
const StyledOl = styled.ol``

const StyledUl = styled.ul``

const StyledLi = styled.li`
  margin: 0.1rem 0;
  padding: 0 0.2rem;
`

interface IListProps {
  style: IListBlock['data']['style']
  items: INestedListItem[]
}

const ListElements = ({ style, items }: IListProps) => (
  <>
    {items.map((item: INestedListItem, index: number) => (
      <StyledLi key={index}>
        <TextWrapper text={item.content} />
        {Array.isArray(item.items) && item.items.length > 0 && (
          <Group style={style} items={item.items} />
        )}
      </StyledLi>
    ))}
  </>
)

function getGroupComponent(style: string) {
  switch (style) {
    case 'ordered':
      return StyledOl

    case 'unordered':
    default:
      return StyledUl
  }
}

function Group({ style, items }: IListProps) {
  const Element = getGroupComponent(style)
  return (
    <Element>
      <ListElements items={items} style={style} />
    </Element>
  )
}

export const List = ({
  data: { style, items },
}: {
  data: IListBlock['data']
}) => (
  <Wrapper>
    <Group style={style} items={items} />
  </Wrapper>
)
