import React from 'react'
import styled, { css } from 'styled-components'

import { IParagraphBlock, TextVariant } from 'src/types/Text'
import { TextWrapper } from './TextWrapper'
import { MAX_WIDTH } from './constants'
import { FONT_SIZE } from 'src/style.main'

const callOutStyle = css`
  margin: 1.6rem;
  padding: 0.3rem 0.5rem;
  text-align: center;
  background: rgba(0, 0, 0, 0.35);
  border: 2px solid black;
  border-radius: 4px;
`
const citationStyle = css`
  margin: 1.4rem;
  padding: 0.6rem 1rem;
  font-style: italic;
  text-align: center;
  background: rgba(0, 0, 0, 0.35);
  border-radius: 4px;
`
const detailsStyle = css`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0.4rem 0.7rem;
  font-size: ${FONT_SIZE.smaller};
  background: rgba(180, 180, 180, 0.08);
`

interface IBlockProps {
  variant?: TextVariant
}

const Block = styled.p<IBlockProps>`
  max-width: ${MAX_WIDTH}px;
  background-color: rgba(0, 0, 0, 0.3);
  padding: 0.5rem 0.75rem;

  ${(props) => {
    const { variant } = props
    switch (variant) {
      case TextVariant.CALL_OUT:
        return callOutStyle

      case TextVariant.CITATION:
        return citationStyle

      case TextVariant.DETAILS:
        return detailsStyle

      default:
        return
    }
  }}
`

interface IProps {
  data: IParagraphBlock['data']
  tunes: IParagraphBlock['tunes']
}

export const Paragraph = ({ data, tunes }: IProps) => (
  <Block variant={tunes?.textVariant}>
    <TextWrapper text={data.text} />
  </Block>
)
