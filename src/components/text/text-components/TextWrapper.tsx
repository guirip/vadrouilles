import React from 'react'
import styled, { css } from 'styled-components'
import { FontFamily } from 'src/fonts'
import { TEXT_COLOR } from 'src/style.main'

export enum TextWrapperVariant {
  ITALIC = 'italic',
  CENTERED = 'centered',
}

interface IStyledTextWrapperProps {
  variants: TextWrapperVariant[]
}

const StyledTextWrapper = styled.span<IStyledTextWrapperProps>`
  font-family: ${FontFamily.MERRIWEATHER}, sans-serif;

  ${({ variants }) =>
    variants.includes(TextWrapperVariant.ITALIC) &&
    css`
      font-style: italic;
    `}

  ${({ variants }) =>
    variants.includes(TextWrapperVariant.CENTERED) &&
    css`
      text-align: center;
    `}

  & a {
    color: #56ffff;
  }
  & a:visited {
    color: #abffff;
  }
  & u {
    text-decoration: underline;
  }
  & mark {
    background-color: ${TEXT_COLOR};
  }
`

interface IProps {
  text: string
  variants?: TextWrapperVariant[]
}

export const TextWrapper = ({ text, variants = [] }: IProps) => (
  <StyledTextWrapper
    variants={variants}
    dangerouslySetInnerHTML={{ __html: text }}
  />
)
