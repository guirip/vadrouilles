import React from 'react'
import styled from 'styled-components'
import { TEXT_COLOR } from 'src/style.main'

const DelimiterWrapper = styled.div`
  display: flex;
  justify-content: center;

  & div {
    margin: 2rem 0;
    width: 45vw;
    border-bottom: 1px solid ${TEXT_COLOR};
    opacity: 0.6;
  }
`

export const Delimiter = () => (
  <DelimiterWrapper>
    <div></div>
  </DelimiterWrapper>
)
