import React, { useCallback, useState } from 'react'
import { IHeaderBlock, ISummaryBlock } from 'src/types/Text'
import styled, { css } from 'styled-components'
import { TextWrapper } from './TextWrapper'
import { TEXT_COLOR } from 'src/style.main'
import { useLangContext } from 'src/context/LangContext'
import { useScrollContext } from 'src/context/ScrollContext'
import { getHeaderId } from './helpers'

const INDENT = 15

// NB: level 1 and 2 are basically never used
// The problem is we have an obvious and useless margin-left in most cases (so cheating with `- 2 * INDENT`)
const StyledSummaryRow = styled.div<{ level: number }>`
  width: fit-content;
  margin-left: ${(props) => (props.level - 1) * INDENT - 2 * INDENT}px;
  padding: 3px;
  font-size: 0.8rem;
  text-decoration: none;
  color: ${TEXT_COLOR};
  opacity: 0.9;
  cursor: pointer;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0.5);
`
const StyledIcon = styled.i`
  font-size: 0.65rem;
  margin-right: 0.3rem;
`

interface ISummaryRowProps {
  header: IHeaderBlock
  scrollTo: (id: string) => void
}

const SummaryRow = ({ header, scrollTo }: ISummaryRowProps) => (
  <StyledSummaryRow
    level={header.data.level}
    onClick={() => scrollTo(header.id)}
  >
    <StyledIcon className="pi pi-angle-right" />
    <TextWrapper text={header.data.text} />
  </StyledSummaryRow>
)

const StyledSummary = styled.div<{ folded: boolean }>`
  display: flex;
  flex-direction: column;
  width: fit-content;
  ${(props) =>
    props.folded
      ? ''
      : css`
          min-width: 250px;
        `}
  margin: 0 auto 2rem;
  background-color: rgba(0, 0, 0, 0.55);
  border: 1px solid #616161;
`
const SummaryHeader = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  align-self: center;
  padding: 0.2rem 0.4rem;
  width: 100%;
  font-size: 0.7rem;
  font-weight: bold;
  opacity: 0.65;
  -webkit-tap-highlight-color: transparent;

  & i:first-child {
    visibility: hidden;
  }
  & span {
    padding: 0 1.4rem;
    text-align: center;
    cursor: pointer;
  }
  & i {
    padding-right: 0.2rem;
    font-size: 1.2rem;
    opacity: 0.5;
    cursor: pointer;
  }
`
const SummaryContent = styled.div`
  margin: 1rem auto;
  padding: 0 1.8rem;
`

interface IProps {
  data: ISummaryBlock['data']
  containerRef: React.RefObject<HTMLDivElement>
}

export function Summary({ data, containerRef }: IProps) {
  const { labels } = useLangContext()
  const { ref: scrollable } = useScrollContext()
  const [folded, setFolded] = useState(false)

  const scrollTo = useCallback(
    (elId: string) => {
      const el = containerRef.current?.querySelector(`#${getHeaderId(elId)}`)

      if (scrollable?.current && el instanceof HTMLHeadingElement) {
        const scrollableOffsetTop = scrollable.current.offsetTop
        const elOffsetTop = el.offsetTop
        const translateY = elOffsetTop - scrollableOffsetTop - 20
        scrollable.current.scrollBy({
          left: 0,
          top: translateY,
          behavior: 'smooth',
        })
      }
    },
    [containerRef, scrollable]
  )

  return !Array.isArray(data.headers) || data.headers.length === 0 ? null : (
    <StyledSummary folded={folded}>
      <SummaryHeader>
        {!folded && <i className="pi pi-times" />}
        <span onClick={() => setFolded(!folded)}>{labels.text.summary}</span>
        {!folded && (
          <i className="pi pi-times" onClick={() => setFolded(!folded)} />
        )}
      </SummaryHeader>

      {folded === false && (
        <SummaryContent>
          {data.headers.map((header) => (
            <SummaryRow key={header.id} header={header} scrollTo={scrollTo} />
          ))}
        </SummaryContent>
      )}
    </StyledSummary>
  )
}
