import React from 'react'
import styled from 'styled-components'
import { IHeaderBlock } from 'src/types/Text'
import { MAX_WIDTH } from './constants'
import { TextWrapper } from './TextWrapper'
import { FONT_SIZE } from 'src/style.main'
import { getHeaderId } from './helpers'

const Wrapper = styled.div`
  max-width: ${MAX_WIDTH}px;

  & > h1 {
    font-size: ${FONT_SIZE.h1};
    margin: 50px 0 26px;
  }
  & > h2 {
    font-size: ${FONT_SIZE.h2};
    margin: 45px 0 22px;
  }
  & > h3 {
    font-size: ${FONT_SIZE.h3};
    margin: 40px 0 19px;
  }
  & > h4 {
    font-size: ${FONT_SIZE.h4};
    margin: 35px 0 15px;
  }
  & > h5 {
    font-size: ${FONT_SIZE.h5};
    margin: 22px 0 10px;
  }
  & > h6 {
    font-size: ${FONT_SIZE.h6};
    margin: 15px 0;
  }
`

const wrapContent = (text: string) => <TextWrapper text={text} />

function getHeader(
  id: IHeaderBlock['id'],
  level: IHeaderBlock['data']['level'],
  text: IHeaderBlock['data']['text']
) {
  switch (level) {
    case 1:
      return <h1 id={getHeaderId(id)}>{wrapContent(text)}</h1>
    case 3:
      return <h3 id={getHeaderId(id)}>{wrapContent(text)}</h3>
    case 4:
      return <h4 id={getHeaderId(id)}>{wrapContent(text)}</h4>
    case 5:
      return <h5 id={getHeaderId(id)}>{wrapContent(text)}</h5>
    case 6:
      return <h6 id={getHeaderId(id)}>{wrapContent(text)}</h6>
    case 2:
    default:
      return <h2 id={getHeaderId(id)}>{wrapContent(text)}</h2>
  }
}

interface IProps {
  id: IHeaderBlock['id']
  data: IHeaderBlock['data']
}

export const Header = ({ id, data: { text, level } }: IProps) => (
  <Wrapper>{getHeader(id, level, text)}</Wrapper>
)
