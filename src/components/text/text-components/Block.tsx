import React from 'react'
import Gallery from 'src/components/gallery'
import {
  IBlock,
  IImageBlock,
  IVideoBlock,
  BlockType,
  isParagraphBlock,
  isHeaderBlock,
  isImageBlock,
  isVideoBlock,
  isDelimiterBlock,
  isGalleryBlock,
  isListBlock,
  isSpacerBlock,
  isSummaryBlock,
  isTableBlock,
} from 'src/types/Text'
import { Paragraph } from './Paragraph'
import { VideoProxy } from 'src/components/video/VideoProxy'
import { Delimiter } from './Delimiter'
import { Header } from './Header'
import { Image } from './Image'
import { List } from './List'
import { Table } from './Table'
import { Spacer } from './Spacer'
import { Summary } from './Summary'
import { GallerySize } from 'src/components/gallery/types'

interface IBlockProps {
  block: IBlock
  containerRef: React.RefObject<HTMLDivElement>
}

function getGalleryContent(block: IImageBlock | IVideoBlock) {
  switch (block.type) {
    case BlockType.Image:
      return block.data.file
    case BlockType.Video:
      return block.data
  }
}

export function Block({ block, containerRef }: IBlockProps) {
  if (isParagraphBlock(block)) {
    return <Paragraph data={block.data} tunes={block.tunes} />
  }
  if (isHeaderBlock(block)) {
    return <Header id={block.id} data={block.data} />
  }
  if (isImageBlock(block)) {
    return <Image data={block.data} />
  }
  if (isVideoBlock(block)) {
    return <VideoProxy data={block.data} />
  }
  if (isTableBlock(block)) {
    return <Table data={block.data} />
  }
  if (isDelimiterBlock(block)) {
    return <Delimiter />
  }
  if (isGalleryBlock(block)) {
    return (
      <Gallery
        visuals={block.data.visuals.map((block) => getGalleryContent(block))}
        size={GallerySize.SMALL}
        showGalleryThumbnails={block.data.showThumbnails === true}
      />
    )
  }
  if (isListBlock(block)) {
    return <List data={block.data} />
  }
  if (isSpacerBlock(block)) {
    return <Spacer data={block.data} />
  }
  if (isSummaryBlock(block)) {
    return <Summary data={block.data} containerRef={containerRef} />
  }

  console.error('unmanaged block type: ' + block.type)
  return null
}
