import React from 'react'
import styled from 'styled-components'
import { useCodifContext } from 'src/context/CodifContext'
import { IImageBlock } from 'src/types/Text'
import { TextWrapper, TextWrapperVariant } from './TextWrapper'

const textVariants = [TextWrapperVariant.ITALIC, TextWrapperVariant.CENTERED]

const Wrapper = styled.div`
  & span {
    padding: 0 1rem;
  }
`
const Photo = styled.img`
  max-width: 72vw;
  max-height: 84vh;
  cursor: pointer;

  @media (max-width: 760px) {
    max-width: 94vw;
    max-height: 70vh;
  }
`

export function Image({ data }: { data: IImageBlock['data'] }) {
  const { staticUrl } = useCodifContext()

  return !staticUrl ? null : (
    <Wrapper className="flex justify-content-center align-items-center flex-column my-4">
      <a href={staticUrl + '/' + data.file.mid}>
        <Photo
          className="align-items-center mt-1 mb-2"
          src={staticUrl + '/' + data.file.mid}
          alt={data.caption}
        />
      </a>
      {data.caption && (
        <TextWrapper text={data.caption} variants={textVariants} />
      )}
    </Wrapper>
  )
}
