import React from 'react'
import { ITableBlock } from 'src/types/Text'
import styled from 'styled-components'
import { LocalizedTextWrapper } from '../LocalizedTextWrapper'
import { TextWrapper } from './TextWrapper'

const BORDER_STYLE = '1px solid #797979'
const TH_BG_COLOR = 'rgba(255, 255, 255, .1)'

const StyledTable = styled.table`
  ${LocalizedTextWrapper} & {
    margin: 3rem auto;
    border-spacing: 0;
  }

  & th {
    position: sticky;
    top: 0;
    padding: 0.6rem 1rem;
    text-align: center;
    background-color: ${TH_BG_COLOR};
  }
  & td {
    padding: 0.4rem 0.6rem;
    max-width: 600px;
    border: ${BORDER_STYLE};
    border-right: 0;
    border-bottom: 0;
  }
  & td:last-child {
    border-right: ${BORDER_STYLE};
  }
  & tr:last-child td {
    border-bottom: ${BORDER_STYLE};
  }
`

type RowData = string[]

const TableHeader = ({ headerData }: { headerData: RowData }) => (
  <thead>
    <tr>
      {headerData.map((heading) => (
        <th>
          <TextWrapper text={heading} />
        </th>
      ))}
    </tr>
  </thead>
)

const TableRow = ({ rowData }: { rowData: RowData }) => (
  <tr>
    {rowData.map((cellData) => (
      <td>
        <TextWrapper text={cellData} />
      </td>
    ))}
  </tr>
)

const TableContent = ({ rows }: { rows: RowData[] }) => (
  <tbody>
    {rows.map((rowData) => (
      <TableRow rowData={rowData} />
    ))}
  </tbody>
)

export const Table = ({
  data: { content, withHeadings },
}: {
  data: ITableBlock['data']
}) => (
  <StyledTable>
    {withHeadings ? (
      <>
        <TableHeader headerData={content[0]} />
        <TableContent rows={content.slice(1)} />
      </>
    ) : (
      <TableContent rows={content} />
    )}
  </StyledTable>
)
