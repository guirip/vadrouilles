import { IHeaderBlock } from 'src/types/Text'

export const getHeaderId = (blockId: IHeaderBlock['id']) => `header-${blockId}`
