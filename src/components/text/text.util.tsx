import { ITextPage } from 'src/types/Text'

export const getDefaultPage = (textPages: ITextPage[]) => textPages[0]

export function getTextPage(slug: string | undefined, textPages: ITextPage[]) {
  if (textPages.length === 0) {
    return null
  }
  if (!slug) {
    return getDefaultPage(textPages)
  }
  return textPages.find((textPage) => textPage.slug === slug)
}
