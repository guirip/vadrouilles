import styled from 'styled-components'

export const LocalizedTextWrapper = styled.div`
  // min-height: 50vh;
  line-height: 1.7rem;
  text-align: justify;

  & > * {
    margin: 0 auto 20px;
  }

  &:after {
    content: '';
    display: inline-block;
    width: 100%;
  }
`
