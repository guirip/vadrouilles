/*
import React from 'react'
import styled from 'styled-components'
import {CommentCount, DiscussionEmbed} from 'disqus-react'

import config from 'src/config'
import { useLangContext } from 'src/context/LangContext'
import { FONT_SIZE } from 'src/style.main'

const ToggleCommentBlock = styled.div`
  padding: 0.9rem 1rem;
  font-weight: bold;
  font-size: ${FONT_SIZE.smallest};
  background-color: rgba(255, 255, 255, 0.4);
  border: 1px solid #aeaeae;
  border-radius: 3px;
  cursor: pointer;
`
const DiscussionEmbedWrapper = styled.div`
  margin: 2.2rem 0.3rem 0.1rem;
  padding: 0.7rem;
  background-color: rgba(255, 255, 255, 0.85);
`

interface IProps {
  visible: boolean
  identifier: string
  title: string
  setBlockVisible: () => void
}

function CommentsBlock({
  visible,
  identifier,
  title,
  setBlockVisible,
}: IProps) {
  const { labels } = useLangContext()
  const disqusConfig = {
    url: window.location.href,
    identifier,
    title,
    language: labels.isoCode,
  }

  if (visible !== true) {
    return (
      <div className="flex justify-content-center mt-5 mx-3">
        <ToggleCommentBlock onClick={setBlockVisible}>
          {// labels.commentsToggle
          }
          <CommentCount
            shortname={config.DISQUS_SHORT_NAME}
            config={disqusConfig}
          />
        </ToggleCommentBlock>
      </div>
    )
  }

  return (
    <DiscussionEmbedWrapper>
      <DiscussionEmbed
        shortname={config.DISQUS_SHORT_NAME}
        config={disqusConfig}
      />
    </DiscussionEmbedWrapper>
  )
}
*/

function CommentsBlock() {
  return null
}

export default CommentsBlock
