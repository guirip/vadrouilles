import React, { useRef, useState } from 'react'
import { Galleria } from 'primereact/galleria'

import { useCodifContext } from 'src/context/CodifContext'
import { IFile, isFile } from 'src/types/File'
import {
  GalleryImage,
  Thumb,
  StyledGalleria,
  PicturesContainer,
  MainPicture,
  ThumbsContainer,
  VideoThumb,
  RESPONSIVE_OPTIONS,
} from './styles'

import './override.css'
import { isVideo, IVideo } from 'src/types/Video'
import { VideoProxy } from '../video/VideoProxy'
import { GallerySize } from './types'
import { stopEventPropagation } from 'src/utils/Utils'

type Visual = IFile | IVideo

interface IGalleryProps {
  visuals?: Visual[]
  size: GallerySize
  showGalleryThumbnails: boolean
}

function Gallery({ visuals, size, showGalleryThumbnails }: IGalleryProps) {
  const { staticUrl } = useCodifContext()

  const galleryRef = useRef<Galleria>(null)
  const [galleryActiveIndex, setGalleryActiveIndex] = useState(0)

  if (!visuals || Array.isArray(visuals) !== true || visuals.length === 0) {
    return null
  }

  const getImageLowUrl = (file: IFile) => `${staticUrl}/${file.low}`
  const getImageMidUrl = (file: IFile) => `${staticUrl}/${file.mid}`

  function onClickOnVisual(index: number) {
    setGalleryActiveIndex(index)
    if (!galleryRef.current) {
      console.error('Gallery not intialized yet')
    } else {
      galleryRef.current.show()
    }
  }
  function onGalleryItemChange(e: { index: number }) {
    setGalleryActiveIndex(e.index)
  }

  const itemTemplate = (visual: Visual) =>
    isVideo(visual) ? (
      <VideoProxy data={{ ...visual, width: 200, height: 300 }} autoplay />
    ) : (
      <GalleryImage src={getImageMidUrl(visual)} alt="" />
    )

  const thumbnailTemplate = (visual: Visual) =>
    isVideo(visual) ? (
      <VideoThumb className="pi pi-video" size={GallerySize.SMALL} />
    ) : (
      <Thumb
        src={getImageLowUrl(visual)}
        size={GallerySize.SMALL}
        onTouchEnd={stopEventPropagation} // Import Galleria component bugfix
      />
    )

  let mainVisual: Visual | undefined
  const visualsWithoutMainAttr: Visual[] = []

  visuals.forEach((visual) => {
    if (isFile(visual) && visual.isMain) {
      mainVisual = visual
    } else {
      visualsWithoutMainAttr.push(visual)
    }
  })

  if (!mainVisual && visualsWithoutMainAttr.length > 0) {
    mainVisual = visualsWithoutMainAttr.shift()
  }

  const galleryFiles = [mainVisual, ...visualsWithoutMainAttr]

  return (
    <>
      <StyledGalleria
        ref={galleryRef}
        value={galleryFiles}
        activeIndex={galleryActiveIndex}
        onItemChange={onGalleryItemChange}
        circular
        fullScreen
        showItemNavigators
        showIndicatorsOnItem
        showThumbnails={showGalleryThumbnails}
        showThumbnailNavigators={false}
        item={itemTemplate}
        thumbnail={thumbnailTemplate}
        responsiveOptions={RESPONSIVE_OPTIONS}
      />
      <PicturesContainer>
        {mainVisual && isVideo(mainVisual) && <VideoProxy data={mainVisual} />}
        {mainVisual && !isVideo(mainVisual) && (
          <MainPicture
            size={size}
            src={getImageMidUrl(mainVisual)}
            onClick={() => onClickOnVisual(0)}
          />
        )}
        <ThumbsContainer>
          {visualsWithoutMainAttr.map((visual, index) =>
            isVideo(visual) ? (
              <VideoThumb
                size={size}
                key={index}
                className="pi pi-video"
                onClick={() => onClickOnVisual(index + 1)}
              />
            ) : (
              <Thumb
                size={size}
                key={index}
                src={getImageLowUrl(visual)}
                onClick={() => onClickOnVisual(index + 1)}
              />
            )
          )}
        </ThumbsContainer>
      </PicturesContainer>
    </>
  )
}

export default Gallery
