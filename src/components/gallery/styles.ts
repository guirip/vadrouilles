import { Galleria } from 'primereact/galleria'
import styled, { css } from 'styled-components'
import { GallerySize } from './types'
import { isMobileDevice, isSafari } from 'src/utils/Utils'
import { VideoContainer } from '../video/EmbeddedVideo'

export const RESPONSIVE_OPTIONS = [
  {
    breakpoint: '10000px',
    numVisible: 14,
  },
  {
    breakpoint: '1280px',
    numVisible: 10,
  },
  {
    breakpoint: '768px',
    numVisible: 8,
  },
  {
    breakpoint: '560px',
    numVisible: 5,
  },
]

export const PicturesContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
`

interface ISizeableProps {
  size: GallerySize
}

export const MainPicture = styled.img<ISizeableProps>`
  max-width: 96vw;
  cursor: pointer;
  -webkit-tap-highlight-color: transparent;
`

export const ThumbsContainer = styled.div`
  display: flex;
  flex-direction: row;
  max-width: 90vw;
  align-items: center;
  margin: 1rem auto;
  overflow-x: auto;

  & img:last-child {
    padding-right: 0;
  }
`

const THUMB_MARGINS = {
  [GallerySize.SMALL]: '3px',
  [GallerySize.MEDIUM]: '6px',
  [GallerySize.BIG]: '8px',
}

const getThumbSizeStyle = (size: GallerySize) => {
  switch (size) {
    case GallerySize.SMALL:
      return `
        width: 60px;
        height: 60px;
      `
    case GallerySize.MEDIUM:
      return `
        width: 80px;
        height: 80px;
      `
    case GallerySize.BIG:
      return `
        width: 180px;
        max-height: 180px;
      `
  }
}

interface IThumbProps extends ISizeableProps {
  src: string
}

export const Thumb = styled.div<IThumbProps>`
  ${({ size }) => getThumbSizeStyle(size)}
  flex-shrink: 0;
  margin: ${({ size }) => THUMB_MARGINS[size]};
  background-image: url(${({ src }) => src});
  background-size: cover;
  background-position: 50% 50%;
  cursor: pointer;
  -webkit-tap-highlight-color: transparent;
`

const getVideoThumbSizeStyle = (size: GallerySize) => {
  switch (size) {
    case GallerySize.SMALL:
      return `
        margin: 2px;
        font-size: 2rem;
      `

    case GallerySize.MEDIUM:
      return `
        margin: 4px;
        font-size: 2.3rem;
      `

    case GallerySize.BIG:
      return `
        margin: 2px;
        font-size: 3rem;
      `
  }
}

export const VideoThumb = styled.span<{ size: GallerySize }>`
  margin: ${({ size }) => THUMB_MARGINS[size]};
  opacity: 0.7;
  cursor: pointer;

  &:before {
    ${({ size }) => getVideoThumbSizeStyle(size)}
    background-color: rgba(0,0,0,0.5);
    padding: 0.5rem 0.7rem;
    border-radius: 7%;
  }
`

export const GalleryImage = styled.img`
  max-width: 98vw;
  max-height: 84vh;
  width: auto;
  height: auto;
`

export const StyledGalleria = styled(Galleria)<{
  showThumbnails: boolean
}>`
  max-width: 100vw;
  max-height: 100vh;
  height: 100%;

  & ${VideoContainer} {
    margin: 10px 0;
  }

  ${({ showThumbnails }) =>
    !showThumbnails &&
    css`
      ${GalleryImage} {
        max-height: ${isSafari() && isMobileDevice() ? '90vh' : '98vh'};
      }
    `}
`
