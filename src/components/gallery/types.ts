export enum GallerySize {
  SMALL = 'small',
  MEDIUM = 'medium',
  BIG = 'big',
}
