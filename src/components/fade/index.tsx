import React from 'react'
import { PropsWithChildren, useEffect, useState } from 'react'
import styled from 'styled-components'

const DEFAULT_DURATION = 1000

interface IProps {
  duration?: number
}

interface IContainerProps {
  visible: boolean
  duration: number
}

const Container = styled.div<IContainerProps>`
  width: 100%;
  height: 100%;
  opacity: ${({ visible }) => (visible ? 1 : 0)};
  transition: opacity ${({ duration }) => duration}ms;
`

export const Fade = ({ children, duration }: PropsWithChildren<IProps>) => {
  const [visible, setVisible] = useState(false)

  useEffect(() => {
    const timeoutId = window.setTimeout(() => {
      setVisible(true)
    }, 1)
    return () => {
      window.clearTimeout(timeoutId)
    }
  }, [])

  return (
    <Container visible={visible} duration={duration ?? DEFAULT_DURATION}>
      {children}
    </Container>
  )
}
