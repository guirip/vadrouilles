import React, { ReactEventHandler } from 'react'
import styled from 'styled-components'
import { DEFAULT_HEIGHT, DEFAULT_WIDTH } from './conf'
import { useSize } from './sizeHook'
import {
  TextWrapper,
  TextWrapperVariant,
} from '../text/text-components/TextWrapper'

export const VideoContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin: 2em 0;

  & iframe {
    margin-bottom: 14px;
    border: 0;
  }
`

const textVariants = [TextWrapperVariant.ITALIC]

interface IProps {
  description: string | undefined
  inputWidth: number | undefined
  inputHeight: number | undefined
  url: string
  allow: string
  title: string
  onLoad?: ReactEventHandler
}

function EmbeddedVideo({
  description,
  inputWidth,
  inputHeight,
  url,
  allow,
  title,
  onLoad,
}: IProps) {
  const { width, height } = useSize(
    inputWidth ?? DEFAULT_WIDTH,
    inputHeight ?? DEFAULT_HEIGHT
  )

  return (
    <VideoContainer>
      <iframe
        width={width}
        height={height}
        src={url}
        allow={allow}
        allowFullScreen
        title={title}
        onLoad={onLoad}
      />
      {description && (
        <TextWrapper text={description} variants={textVariants} />
      )}
    </VideoContainer>
  )
}

export default EmbeddedVideo
