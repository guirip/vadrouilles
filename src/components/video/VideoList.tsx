import React, { useState } from 'react'
import { IVideo } from 'src/types/Video'
import { VideoProxy } from './VideoProxy'

interface IProps {
  videos: IVideo[] | null
}

export function VideoList({ videos }: IProps) {
  const [visibleCount, setVisibleCount] = useState(1)

  function loadNext() {
    window.setTimeout(setVisibleCount, 0, visibleCount + 1)
  }
  return (
    <>
      {(videos || []).map((video, index) =>
        index < visibleCount ? (
          <VideoProxy key={video._id} data={video} onLoad={loadNext} />
        ) : null
      )}
    </>
  )
}
