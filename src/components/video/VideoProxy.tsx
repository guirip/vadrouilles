import React from 'react'
import { IVideo, VideoSource } from 'src/types/Video'
import VimeoVideo from './VimeoVideo'
import YouTubeVideo from './YouTubeVideo'
import DailyMotionVideo from './DailyMotionVideo'

interface IVideoProxyProps {
  data: Pick<IVideo, 'source' | 'value' | 'width' | 'height' | 'description'>
  autoplay?: boolean
  onLoad?: () => void
}

export function VideoProxy({ data, autoplay, onLoad }: IVideoProxyProps) {
  const { source, value, width, height, description } = data

  const videoProps = {
    id: value,
    description,
    width,
    height,
    onLoad,
    autoplay,
  }

  switch (source) {
    case VideoSource.Youtube:
      return <YouTubeVideo {...videoProps} />

    case VideoSource.Vimeo:
      return <VimeoVideo {...videoProps} />

    case VideoSource.Dailymotion:
      return <DailyMotionVideo {...videoProps} />

    default:
      console.error(`Unmanaged video source: ${source}`, data)
      return null
  }
}
