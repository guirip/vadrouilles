import { useEffect, useState } from 'react'

import { debounce } from 'lodash'

const MAX_WIDTH = 1080
const MAX_HEIGHT = 1080

function getClientWidth() {
  const clientWidth = document.documentElement.clientWidth - 30
  if (clientWidth > MAX_WIDTH) {
    return MAX_WIDTH
  }
  return clientWidth
}

function getClientHeight() {
  const clientHeight = document.documentElement.clientHeight - 30
  if (clientHeight > MAX_HEIGHT) {
    return MAX_WIDTH
  }
  return clientHeight
}

const computeSize = (inputWidth: number, inputHeight: number) => {
  const ratio = inputWidth / inputHeight
  const maxWidth = getClientWidth()
  const maxHeight = getClientHeight()

  const widthPercent = (inputWidth * 100) / maxWidth
  const heightPercent = (inputHeight * 100) / maxHeight

  const scaleWidth = widthPercent > heightPercent

  if (scaleWidth) {
    const targetWidth = maxWidth
    return {
      width: targetWidth,
      height: targetWidth / ratio,
    }
  } else {
    const targetHeight = maxHeight
    return {
      height: targetHeight,
      width: targetHeight * ratio,
    }
  }
}

export const useSize = (inputWidth: number, inputHeight: number) => {
  const size = computeSize(inputWidth, inputHeight)
  const [width, setWidth] = useState(size.width)
  const [height, setHeight] = useState(size.height)

  useEffect(() => {
    const onWindowResize = debounce(() => {
      const { width, height } = computeSize(inputWidth, inputHeight)
      // console.log(`RESIZING //  width:${width}  height:${height}`)

      setWidth(width)
      setHeight(height)
    }, 500)

    window.addEventListener('resize', onWindowResize)

    return function cleanup() {
      window.removeEventListener('resize', onWindowResize)
    }
  })

  return { width, height }
}
