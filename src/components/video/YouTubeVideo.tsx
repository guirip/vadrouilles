import React, { lazy, ReactEventHandler } from 'react'

const EmbeddedVideo = lazy(() => import('./EmbeddedVideo'))

interface IProps {
  id: string
  description?: string
  width?: number | null
  height?: number | null
  onLoad?: ReactEventHandler
  autoplay?: boolean
}

function YouTubeVideo({
  id,
  description,
  width,
  height,
  onLoad,
  autoplay = false,
}: IProps) {
  return (
    <EmbeddedVideo
      url={`https://www.youtube.com/embed/${id}?autoplay=${autoplay ? 1 : 0}`}
      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
      description={description ?? undefined}
      inputWidth={width ?? undefined}
      inputHeight={height ?? undefined}
      title={`embedded youtube video ${id}`}
      onLoad={onLoad}
    />
  )
}

export default YouTubeVideo
