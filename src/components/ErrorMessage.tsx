import React from 'react'
import styled from 'styled-components'

const StyledErrorMessage = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
  align-items: center;
  font-style: normal;
  font-weight: bold;
  letter-spacing: 1px;

  & small {
    font-style: italic;
  }
`

interface IProps {
  text: string | JSX.Element
  errMessage?: unknown
}

const ErrorMessage = ({ text, errMessage }: IProps) => (
  <StyledErrorMessage>
    <span className="m-5">{text || ''}</span>
    {errMessage ? (
      <small className="mb-3">({JSON.stringify(errMessage)})</small>
    ) : null}
  </StyledErrorMessage>
)

export default ErrorMessage
