import frLabels from './fr'
import enLabels from './en'
import { Lang, SUPPORTED_LANG } from './Lang'

const LANG_LS_KEY = 'lang'

const defaultLang = Lang.en

const labelsPerLang = {
  [Lang.fr]: frLabels,
  [Lang.en]: enLabels,
}

let currentLang: Lang

function isLangSupported(value: string | Lang): value is Lang {
  if (typeof value !== 'string') {
    return false
  }
  return SUPPORTED_LANG.indexOf(value as Lang) !== -1
}

export function setCurrentLang(value: Lang) {
  if (isLangSupported(value) !== true) {
    console.error(`Unsupported lang: ${value}`)
    return
  }
  currentLang = value
  localStorage.setItem(LANG_LS_KEY, value)
}

export function getCurrentLang() {
  return currentLang
}

export function getLabels(forcedLang?: Lang) {
  return labelsPerLang[forcedLang || currentLang]
}

;(function init() {
  const paramLang: string | Lang =
    localStorage.getItem(LANG_LS_KEY) || window.navigator.language.slice(0, 2)
  if (isLangSupported(paramLang)) {
    setCurrentLang(paramLang)
  } else {
    setCurrentLang(defaultLang)
  }
})()
