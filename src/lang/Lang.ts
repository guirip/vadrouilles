export enum Lang {
  fr = 'fr',
  en = 'en',
}
export const SUPPORTED_LANG = Object.values(Lang)
