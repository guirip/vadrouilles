import { FontFamily } from 'src/fonts'
import styled from 'styled-components'

export const TEXT_COLOR = 'white'
export const BG_COLOR = 'black'
export const SUBTITLE_COLOR = '#b8b8b8'

export const FONT_SIZE = {
  h1: '1.6rem',
  h2: '1.5rem',
  h3: '1.4rem',
  h4: '1.3rem',
  h5: '1.2rem',
  h6: '1.1rem',
  content: '1.2rem',
  smaller: '1rem',
  smallest: '.9rem',
}

export const Page = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100%;
  font-family: ${FontFamily.MONTSERRAT}, sans-serif;
  font-weight: 300;
  color: ${TEXT_COLOR};
`

export const PageContent = styled.div`
  display: flex;
  justify-content: center;
  flex-shrink: 0;
  margin: auto;
`

export const ScrollableContent = styled.div`
  width: 100%;
  overflow-y: auto;
  -webkit-overflow-scrolling: touch;
`

export const Text = styled.div`
  padding: 0.5em;
  font-style: normal;
  font-size: ${FONT_SIZE.content};
`

export const TextBlock = styled(Text)`
  height: fit-content;
  margin: 1em 0.5em;
  padding: 0.6em 0.8em;
  text-align: center;
  background-color: rgba(0, 0, 0, 0.45);

  & a {
    color: #56d5ff;
  }
`
